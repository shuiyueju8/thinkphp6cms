<?php
namespace app\common\upload;

/**
 * Created by JetBrains PhpStorm.
 * User: 上善若水
 * Date: 2019-12-27
 * Time: 上午11: 32
 * UEditor编辑器通用上传七牛玉类
 */
use app\admin\facade\Qiniu;
class QiniuUeditorUploaderTool extends UeditorUploaderTool
{

    /**
     * 构造函数
     * @param string $fileField 表单名称
     * @param array $config 配置项
     * @param bool $base64 是否解析base64编码，可省略。若开启，则$fileField代表的是base64编码的字符串表单名
     */
    public function __construct($fileField, $config, $type = "upload")
    {
        parent::__construct($fileField, $config, $type);
        $this->uploadQiniu();
    }

    /**
     * 将图片上传至七牛云
     */
    protected function uploadQiniu(){
        if($this->stateInfo=='SUCCESS'){//若文件上传成功,者将图片上传至七牛云
            $filePath='.'.$this->fullName;//前面加.为相对路径 ./storage/ueditor/image/20191227/1577430363308693.jpg
            $filename=str_replace('/storage/','',$this->fullName); //上传的文件名去掉/storage/  为ueditor/image/20191227/1577430363308693.jpg
            $url=Qiniu::upload($filePath,$filename,$this->oriName,'ueditor上传');
            if($url){
                $this->fullName=$url;
            }else{
                $this->stateInfo = $this->stateMap['ERROR_UPLOAD_QINIU'];//图片上传至七牛七牛云失败
            }
        }
    }

}