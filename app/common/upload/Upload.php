<?php
namespace app\common\upload;

class Upload
{
    private static function getUpload()
    {
        if(conf('system','qiniuyun')){
            return new QiniuUpload(); //图片上传至本地以及七牛云
        }else{
            return new LocalUpload();//图片上传至本地
        }
    }

    public static function upload($type='远程抓取')
    {
        $uploader=self::getUpload();
        return $uploader->upload($type);
    }

    public static function uploadLink($url,$type='远程抓取')
    {
        $uploader=self::getUpload();
        return $uploader->uploadLink($url,$type);
    }
}