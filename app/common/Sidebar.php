<?php


namespace app\common;

use app\admin\model\Blogroll;
use app\news\model\News;
use app\news\model\Tags;

/**
 * 前端页面右边侧边栏
 * Class Sidebar
 * @package app\common
 */
class Sidebar
{
    protected $news_model;
    protected $tags_model;
    protected $html='';
    protected $sidebar_list=array(
        'recent_update'=>'最近更新',
        'recent_browse'=>'点击排名',
        'site_recommend'=>'站长推荐',
        'think_you_like'=>'猜你喜欢',
        'blogroll'=>'博客链接',
        'hot_tags'=>'热门标签',
        'card'=>'我的名片',
        'tongji_base'=>'站点信息',
        'ad'=>'AD.'
    );
    /**
     * 侧边栏配置
     * @var array
     */
    public $options = [
        'sidebar_list_name'=>array(),
        'sidebar_list_add'=>array(),
    ];
    public function __construct()
    {

    }

    /**
     * 设置$sidebar_list的名字
     * @param $key
     * @param $value
     * @return Sidebar
     */
    public function set_sidebar_name($key,$value){
        if(isset($this->sidebar_list[$key])){
            $this->sidebar_list[$key]=$value;
        }
        return $this;
    }

    /**
     * 添加sidebar
     * 调用方式add_sidebar('recent_update',8,2)
     * 调用方式add_sidebar(array('recent_update'=>array('limit'=>8,'news_category_id'=>2),'think_you_like'=>array('limit'=>8,'news_category_id'=>2)))
     * @param array $sidebar
     * @return Sidebar
     */
    public function add_sidebar(array $sidebar){
        if(is_array($sidebar)){
            foreach ($sidebar as $key=>$bar){
                if(is_array($bar)){
                    if(isset($bar['sidebar'])&&in_array($bar['sidebar'],array_keys($this->sidebar_list))){
                        $this->options['sidebar_list_name'][]=$bar['sidebar'];
                        $this->options['sidebar_list_add'][]=$bar;
                    }
                }
            }
        }
        return $this;
    }

    public function remove_sidebar(...$sidebar){
        $args = func_get_args();
        $args=array_unique(array_intersect($args,array_keys($this->sidebar_list)));
        foreach ($args as $arg){
            unset($this->options['sidebar_list_add'][$arg]);
        }
        return $this;
    }


    public function render(){
        $html='';
        foreach ($this->options['sidebar_list_add'] as $key=>$conf){
            $sidebar=$this->options['sidebar_list_name'][$key];
            $sidebar_name=$this->sidebar_list[$sidebar];
            $sidebar_html=call_user_func(array($this, $sidebar),$sidebar_name,$conf);
            $html.=$sidebar_html;
        }
        return $html;
    }

    protected function recent_update($title,$conf){
        $model=$this->get_news_model();
        $recent_update_news=$model->recent_update($conf);
        $html="<div class='whitebg notice'>"
            ."<h2 class='htitle'>{$title}</h2>"
            .'<ul>';
        foreach ($recent_update_news as $vo){
            $url=url('article.index',['id'=>$vo['id']]);
            $html.="<li><a href='{$url}' title='{$vo['title']}' target='_blank'>{$vo['title']}</a></li>";
        }
        $html.='</ul></div>';
        return $html;
    }
    protected function recent_browse($title,$conf){
        $model=$this->get_news_model();
        $recent_browse_news=$model->recent_browse($conf);
        $url=url('article.index',['id'=>$recent_browse_news[0]['id']]);
        $html="<div class='whitebg paihang'>"
            ."<h2 class='htitle'>{$title}</h2>"
            ."<section class=\"topnews imgscale\"><a href=\"{$url}\">"
            ."<img src=\"{$recent_browse_news[0]['cover']}\"><span>{$recent_browse_news[0]['title']}</span></a>"
            .'</section><ul>';
        foreach ($recent_browse_news[1] as $vo){
            $url=url('article.index',['id'=>$vo['id']]);
            $html.="<li><a href='{$url}' title='{$vo['title']}' target='_blank'>{$vo['title']}</a></li>";
        }
        $html.='</ul></div>';
        return $html;
    }
    protected function site_recommend($title,$conf){
        $model=$this->get_news_model();
        $site_recommend_news=$model->site_recommend($conf);
        $url=url('article.index',['id'=>$site_recommend_news[0]['id']]);
        $html="<div class='whitebg tuijian'>"
            ."<h2 class='htitle'>{$title}</h2>"
            ."<section class=\"topnews imgscale\"><a href=\"{$url}\">"
            ."<img src=\"{$site_recommend_news[0]['cover']}\"><span>{$site_recommend_news[0]['abstract']}</span></a>"
            .'</section><ul>';
        foreach ($site_recommend_news[1] as $vo){
            $url=url('article.index',['id'=>$vo['id']]);
            $html.="<li><a href='{$url}' title='{$vo['title']}' target='_blank'>"
                ."<i><img src=\"{$vo['cover']}\"></i>"
                ."<p>{$vo['abstract']}</p>"
                ."</a></li>";
        }
        $html.='</ul></div>';
        return $html;
    }
    protected function think_you_like($title,$conf){
        $model=$this->get_news_model();
        $think_you_like=$model->think_you_like($conf);
        $html="<div class='whitebg wenzi'>"
            ."<h2 class='htitle'>{$title}</h2>"
            .'<ul>';
        foreach ($think_you_like as $vo){
            $url=url('article.index',['id'=>$vo['id']]);
            $html.="<li><a href='{$url}' title='{$vo['title']}' target='_blank'>{$vo['title']}</a></li>";
        }
        $html.='</ul></div>';
        return $html;
    }
    protected function blogroll($title,$conf){
        $blogroll = Blogroll::where('status',1)->order(array('sorts','id'=>'desc'))->select();
        $html="<div class='whitebg links'>"
            ."<h2 class='htitle'>{$title}</h2>"
            .'<ul>';
        foreach ($blogroll as $vo){
            $url=$vo['blogroll'];
            $html.="<li><a href=\"{$url}\" title=\"{$vo['name']}\" target=\"_blank\">{$vo['name']}</a></li>";
        }
        $html.='</ul></div>';
        return $html;
    }
    protected function hot_tags($title,$conf){
        $tags=Tags::hot_tags();
        $html="<div class='whitebg cloud'>"
            ."<h2 class='htitle'>{$title}</h2>"
            .'<ul>';
        foreach ($tags as $vo){
            $url=url('tag.index',['name'=>$vo['name'],'page'=>1]);
            $html.="<a href=\"{$url}\" target=\"_blank\">{$vo['name']}</a>";
        }
        $html.='</ul></div>';
        return $html;
    }
    protected function card($title,$conf){
        $confs=conf();
        $html="<div class=\"card\">"
            ."<h2>我的名片</h2>"
            ."<p>网名：{$confs['card']['nickname']}</p>"
            ."<p>职业：{$confs['card']['job']}</p>"
            ."<p>现居：{$confs['card']['address']}</p>"
            ."<p>Email：{$confs['card']['email']}</p>"
            ."<ul class=\"linkmore\">"
            ."<li><a href=\"https://www.jianshu.com/u/d1524f5b2a69\" target=\"_blank\" class=\"iconfont icon-jianshu\" title=\"访问我的简书首页\"></a></li>"
            ."<li><a href=\"http://mail.qq.com/cgi-bin/qm_share?t=qm_mailme&email=zqinq7y3pbeip6COqKG2o6_nouCtoaM\" target=\"_blank\" class=\"iconfont icon-foxmail\" title=\"点击给我发送邮箱\"></a></li>"
            ."<li><a href=\"https://gitee.com/singliang/tp6-xadmin\" target=\"_blank\" class=\"iconfont icon-mayun\" title=\"码云仓库\"></a></li>"
            ."<li id=\"weixin\"><a href=\"#\" target=\"_blank\" class=\"iconfont icon-weixin\" title=\"关注我的微信公众号\"></a><i><img src=\"{$confs['base']['weixin_image']}\"></i></li>"
            ."</ul></div>";
        return $html;
    }
    protected function tongji_base($title,$conf){
        $base=conf('base');
        $html="<div class=\"whitebg tongji\">"
            ."<h2 class=\"htitle\">{$title}</h2>"
            ."<ul>"
            ."<li><b>建站时间</b>：{$base['build']}</li>"
            ."<li><b>网站程序</b>：{$base['framework']}</li>"
            ."<li><b>文章统计</b>：<a href=\"javascript:;\" target=\"_blank\">{$base['article_total']}</a>篇文章</li>"
            ."<li><b>标签管理</b>：<a href=\"javascript:;\" target=\"_blank\">标签云</a></li>"
            ."<li><b>统计数据</b>：<a href=\"https://new.cnzz.com/v1/login.php?siteid=1278291546\" target=\"_blank\">cnzz统计</a></li>"
            ."<li><b>微信公众号</b>：扫描二维码，关注我们</li>"
            ."<li class=\"tongji_gzh\"><img src='{$base['weixin_image']}'></li>"
            ."</ul>"
            ."</div>";
        return $html;
    }
    protected function ad($title,$conf){
        $html="<div class=\"ad whitebg imgscale\">"
            ."<ul><a href='{$conf['url']}' target=\"_blank\"><img src='{$conf['cover']}'></a></ul>"
            ."</div>";
        return $html;
    }
    protected function get_news_model(){
        if($this->tags_model){
            return $this->news_model;
        }else{
            return new News();
        }
    }
}