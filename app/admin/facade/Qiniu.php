<?php


namespace app\admin\facade;
use think\Facade;

class Qiniu extends Facade
{
    protected static function getFacadeClass()
    {
        return 'app\common\Upload\QiniuUpload';
    }
}