<?php

namespace app\admin\controller;

use app\admin\model\Blogroll as Blogrolls;
use app\admin\validate\Blogroll as BlogrollsValidate;
use app\admin\traits\ControllerTrait;

/**
 * admin基础控制器
 * @author duqiu
 */
class Blogroll extends AdminBase
{
    protected $m;   //当前控制器关联模型
    protected $v;

    use ControllerTrait;
    public function initialize()
    {
        parent::initialize();
        $this->m = new Blogrolls;   //别名：避免与控制名冲突
        $this->v = new BlogrollsValidate;   //别名：避免与控制名冲突
    }

    public function tree($page = 1, $limit = 10)
    {
        $count=$this->m->count();
        $data=$this->m->order(array('sorts','id'=>'desc'))->page($page,$limit)->select();
        return json([
            'code'=>0,
            'msg'=>'查询成功',
            'count'=>$count,
            'data'=>$data
        ]);
    }

}