<?php
namespace app\admin\controller;

use app\admin\model\AuthRule as AuthRules;
use app\admin\traits\ControllerTrait;
use app\admin\validate\AuthRule as AuthRuleValidate;
use think\facade\View;
use think\Request;

class AuthRule extends AdminBase
{
    use ControllerTrait;
    protected function initialize()
    {
        parent::initialize();
        $this->m = new AuthRules;   //别名：避免与控制名冲突
        $this->v=new AuthRuleValidate;   //创建一个验证类
    }

    public function index()
    {
        return View::fetch();
    }
    //权限管理首页获取所有节点树
    public function tree2()
    {
        $dataList = $this->m->treeList2();
        return json(['msg'=>'success','code'=>0,'data'=>$dataList,'count'=>count($dataList),'is'=>true,'tip'=>'操作成功']);
    }

    //获取所有权限列表树，添加用户时调用
    public function tree()
    {
        return json($this->m->treeList3());
    }

    /**
     * 保存新建的资源
     *
     * @param  \think\Request  $request
     * @return \think\Response
     */
    public function add()
    {
        if ($this->request->isPost()){
            $data = input('post.');
            if (! $this->v->scene('add')->check($data)) {
                $this->result(null,400,$this->v->getError());
            }
            $data['link']=$data['name'];
            $result = $this->m->create($data);
            if ($result){
                return json(['code'=>0,'msg'=>'成功']);
            }else{
                return json(['code'=>400,'msg'=>'保存失败']);
            }
        }
        $treeList = $this->m->treeList();
        return view('add',['treeList'=>$treeList]);
    }
    /**
     * 复制当前节点并编辑
     *
     * @param  \think\Request  $request
     * @return \think\Response
     */
    public function copy(Request $request)
    {
        if ($request->isPost()){
            $data = input('post.');
            if (! $this->v->scene('add')->check($data)) {
                $this->result(null,400,$this->v->getError());
            }
            $data['link']=$data['name'];
            $result = $this->m->create($data);
            if ($result){
                return json(['code'=>0,'msg'=>'成功']);
            }else{
                return json(['code'=>400,'msg'=>'保存失败']);
            }
        }
        $current=$this->m->find($request->get('id'));
        $treeList = $this->m->treeList();
        return view('copy',compact('current','treeList'));
    }

    public function edit($id)
    {
        if ($this->request->isPost()){
            $data = input('post.');
            if (! $this->v->scene('edit')->check($data)) {
                $this->result(null,400,$this->v->getError());
            }
            $this->m=$this->m->find($id);
            $data['link']=$data['name'];
            $result = $this->m->save($data);
            if ($result){
                return json(['code' => 0, 'msg' => '成功', 'data' => null]);
            }else{
                return ajaxReturn($this->v->getError());
            }
        }else{
            $data = $this->m->find($id);
            $treeList = $this->m->treeList();
            return view('edit',compact('treeList','data'));
        }
    }

    public function delete($id)
    {
        if (request()->isPost()) {
            if(is_array($id)){//批量删除

            }else{//单选删除
                $list=$this->m->treeList2($id);//获取该节点下所有节点数组
                $id=[$id];
                if($list){//存在子节点则合并
                    $id=array_merge($id,array_column($list,'id'));
                }
            }
            //级联删除该id下的所有节点
            $result = AuthRules::destroy($id);
            if ($result) {
                return json(['code' => 0, 'msg' => '成功删除'.count($id).'条记录', 'data' => null]);
            } else {
                return json(['code' => 404, 'msg' => '删除失败', 'data' => null]);
            }
        }
    }

}