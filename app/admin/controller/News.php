<?php

namespace app\admin\controller;

use app\admin\model\News as Newss;
use app\admin\model\Tags;
use app\admin\traits\ControllerTrait;
use app\admin\validate\News as NewssValidate;
use think\Request;

class News extends AdminBase
{
    use ControllerTrait;
    public function initialize()
    {
        parent::initialize();
        $this->m = new Newss;   //别名：避免与控制名冲突
        $this->v=new NewssValidate;   //创建一个验证类
    }

    public function index()
    {
        $newsCategory= new \app\admin\model\NewsCategory();
        $treeList = $newsCategory->tree();
        return view('index',compact('treeList'));
    }

    public function add()
    {
        if (request()->isPost()) {
            $data = input('post.');
            $result = $this->m->create($data);
            if(isset($data['tags'])&&$data['tags']){
                $tags=Tags::uniqueSave($data['tags']);
                $result->tags()->attach(array_column($tags,'id'));
            }
            if ($result) {
                return xjson();
            } else {
                return xjson(404);
            }
        }
        $newsCategory= new \app\admin\model\NewsCategory();
        $treeList = $newsCategory->tree();
        if(conf('system','markdown')){
            return view('addUseMarkDown',compact('treeList'));
        }
        return view('add',compact('treeList'));
    }

    public function edit($id)
    {
        if ($this->request->isPost()) {
            $data = $this->request->post();
            if (!$this->v->scene('edit')->check($data)) {
                return xjson(404, $this->v->getError());
            }
            $newss = Newss::find($id);
            $result = $newss->save($data);
            //先删除
            $newss->tags()->detach();
            //再添加
            if(isset($data['tags'])){
                $tags=Tags::uniqueSave($data['tags']);
                $newss->tags()->attach(array_column($tags,'id'));
            }
            if ($result) {
                return xjson();
            } else {
                return xjson( 404);
            }
        } else {
            $data = $this->m->with('tags')->where('id',$id)->find();
            $newsCategory= new \app\admin\model\NewsCategory();
            $treeList = $newsCategory->tree();
            if($data->editor_type){
                return view('editUseMarkDown',compact('data','treeList'));
            }
            return view('edit',compact('data','treeList'));
        }
    }

    public function delete($id)
    {
        if (request()->isPost()) {
            //超级管理员不能删除
            if($id==1||strpos($id,'1,'!==false)){
                return xjson(404, '超级管理员不能删除');
            }
            $result = $this->m->where('id','in',$id)->delete();
            if ($result) {
                return xjson();
            } else {
                return xjson( 404, $this->v->getError());
            }
        }
    }

    public function tree($page = 1, $limit = 10)
    {
        $where=array();
        if(isset($_GET['title'])&&$_GET['title']){
            $where[]=array("title","like","%{$_GET['title']}%");
        }
        if(isset($_GET['top'])&&$_GET['top']){
            $where[]=array("top","=",1);
        }
        if(isset($_GET['hot'])&&$_GET['hot']){
            $where[]=array("hot","=",1);
        }
        if(isset($_GET['news_category_id'])&&$_GET['news_category_id']){
            $newsCategory= new \app\admin\model\NewsCategory();
            $treeList = $newsCategory->select();
            $news=unlimitedSort($treeList,$_GET['news_category_id']);
            $news_category_ids=array_merge(array($_GET['news_category_id']),array_column($news,'id'));
            $where[]=array("news_category_id","in",$news_category_ids);
        }
        $count=$this->m->where($where)->count();
        $data=$this->m->where($where)->with('newsCategory')->order('top desc,id desc')->page($page,$limit)->select();
        return json([
            'code'=>0,
            'msg'=>'查询成功',
            'count'=>$count,
            'data'=>$data
        ]);
    }
}