<?php

namespace app\admin\controller;

use app\admin\util\CollectUtil;
use app\Request;
use think\facade\View;
use QL\QueryList;
class CollectRule extends AdminBase
{
    public function initialize()
    {
        parent::initialize();
        $this->m = new \app\admin\model\collect\CollectRule();   //别名：避免与控制名冲突
        $this->v = new \app\admin\validate\collect\CollectRule();   //别名：避免与控制名冲突
    }
    public function index()
    {
        return View::fetch("collect/collect_rule/index");
    }

    public function tree(Request $request)
    {
        $page=$request->param("page")?$request->param("page"):1;
        $limit=$request->param("limit")?$request->param("limit"):10;
        return json([
            'code'=>0,
            'msg'=>'查询成功',
            'count'=>$this->m->count(),
            'data'=>$this->m->order("sorts")->page($page,$limit)->select()
        ]);
    }

    public function add()
    {
        return View::fetch("collect/collect_rule/add");
    }
    public function edit(Request $request)
    {
        $id=$request->get("id");
        $collect_rule=$this->m->find($id);
        return View::fetch("collect/collect_rule/edit_type".$collect_rule['type'],compact("collect_rule"));
    }

    /**
     * 简单采集提交保存
     * @return \think\Response
     */
    public function add_type1(){
        if (request()->isPost()) {
            $data = input('post.');
            if ($this->v && !$this->v->scene('add_type1')->check($data)) {
                return xjson(404, $this->v->getError());
            }
            $result = $this->m->create($data);
            if ($result) {
                return xjson();
            } else {
                return xjson(404, '添加失败！');
            }
        }
    }

    /**
     * 分片采集提交保存
     * @return \think\Response
     */
    public function add_type2(){
        if (request()->isPost()) {
            $data = input('post.');
            if ($this->v && !$this->v->scene('add_type2')->check($data)) {
                return xjson(404, $this->v->getError());
            }
            $result = $this->m->create($data);
            if ($result) {
                return xjson();
            } else {
                return xjson(404, '添加失败！');
            }
        }
    }

    /**
     * 修改简单采集
     * @return \think\Response
     */
    public function edit_type1(){
        $id = input('get.id');
        $data = input('post.');
        if ($this->v && !$this->v->scene('add_type1')->check($data)) {
            return xjson(404, $this->v->getError());
        }
        $result = $this->m->find($id);
        $result=$result->save($data);
        if ($result) {
            return xjson();
        } else {
            return xjson(404, '更新成功失败！');
        }

    }
    public function edit_type2(){
        $id = input('get.id');
        $data = input('post.');
        if ($this->v && !$this->v->scene('add_type2')->check($data)) {
            return xjson(404, $this->v->getError());
        }
        $result = $this->m->find($id);
        $result=$result->save($data);
        if ($result) {
            return xjson();
        } else {
            return xjson(404, '更新成功失败！');
        }
    }


    public function preview()
    {
        $data = $this->request->param("data");
        $preview = json_decode($data, true);
        if (empty($preview['rules'])) {
            echo "请填写采集规则";
            die();
        }
        $result=CollectUtil::collect_type_simple($preview['url'],$preview['remove'],$preview['encoding'],$preview['rules']);
        dump($result);
        die();
    }

    /**
     * 分片采集预览
     */
    public function previewRange()
    {
        $data = $this->request->param("data");
        $preview = json_decode($data, true);
        if (empty($preview['rules'])) {
            echo "请填写采集规则";
            die();
        }

        $result=CollectUtil::collect_type_range($preview['url'],$preview['remove'],$preview['encoding'],$preview['rules'],$preview['range'],$preview['take']);
        dump($result);
        die();
    }

    public function delete($id)
    {
        if (request()->isPost()) {
            $result = $this->m->where('id','in',$id)->delete();
            if ($result) {
                return xjson( '删除成功！');
            } else {
                return xjson(404, '删除失败！');
            }
        }
    }
}