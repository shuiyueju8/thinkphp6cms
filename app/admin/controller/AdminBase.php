<?php
declare (strict_types = 1);

namespace app\admin\controller;

use app\BaseController;
use think\facade\View;

class AdminBase extends BaseController
{
    protected $middleware = ['auth'];//路由中间件
    protected $m;   //当前控制器关联模型
    protected $v;   //当前控制器验证器
    protected function initialize()
    {
        parent::initialize();
        View::assign(['site_title'=>'tp6网站！','html_title'=>'test','crumbs'=>'crumbs','site'=>'tp6']);
    }
}
