<?php

namespace app\admin\controller;

use app\admin\model\User as Users;
use app\admin\traits\ControllerTrait;
use app\admin\validate\User as UserValidate;
use think\App;
use think\db\exception\DataNotFoundException;
use think\db\exception\DbException;
use think\db\exception\ModelNotFoundException;
use think\facade\View;
use think\Request;
use think\response\Json;

class User extends AdminBase
{
    use ControllerTrait;
    protected $group;
    protected function initialize()
    {
        parent::initialize();
        $this->m = new Users;   //别名：避免与控制名冲突
        $this->v = new UserValidate;   //别名：避免与控制名冲突
        $this->group=new \app\admin\model\AuthGroup();
    }

    public function index()
    {
        return View::fetch();
    }

    public function add()
    {
        if (request()->isPost()) {
            $data = input('post.');
            if (!$this->v->scene('add')->check($data)) {
                return json(['code' => 404, 'msg' => $this->v->getError(), 'data' => null]);
            }
            $result = $this->m->create($data);
            $result->userGroup()->saveAll($data['groups']);
            if ($result) {
                return json(['code' => 0, 'msg' => '成功', 'data' => null]);
            } else {
                return json(['code' => 0, 'msg' => $this->v->getError(), 'data' => null]);
            }
        }
        $groups=$this->group->select();
        View::assign(compact('groups'));
        return View::fetch();
    }

    public static function Menu(){
        $user=Users::with('userGroup')->find(session('uid'));
        $rules=[];
        foreach ($user['userGroup'] as $group){
            $rules=array_merge($rules,explode(',',$group['rules']));
        }
       $menus= \app\admin\model\AuthRule::getMenu($rules);
       return json(["status"=>0,"msg"=>"ok","data"=>$menus]);
    }
    public function tree($page = 1, $limit = 10)
    {
        return json([
            'code'=>0,
            'msg'=>'查询成功',
            'count'=>$this->m->count(),
            'data'=>$this->m->with(['userGroup'=>function($query){
                return $query->where('auth_group.id',1);
            }])->page($page,$limit)->select()
        ]);
        return $this->m->with(['userGroup'=>function($query){
            return $query->where('id',1)->filed('title');
        }])->pageTable($page, $limit);
    }

    /*
     * 用户用户id获取选中的权限节点list
     */
    public function getUserRule($id)
    {
        $user = $this->m->find($id);
        $rule = new \app\admin\model\AuthRule();
        return json($rule->getUserRule($user['rules']));

    }

    public function edit($id)
    {
        if ($this->request->isPost()) {
            $data = $this->request->post();
            if (!$this->v->scene('edit')->check($data)) {
                return json(['code' => 404, 'msg' => $this->v->getError(), 'data' => null]);
            }
            $user = Users::find($id);
            $result = $user->save($data);
            if ($result) {
                return json(['code' => 0, 'msg' => '成功', 'data' => null]);
            } else {
                return json(['code' => 0, 'msg' => $this->v->getError(), 'data' => null]);
            }
        } else {
            $user=$this->m->with('userGroup')->find($id);
            $group_checked_id=array_column($user->userGroup->toArray(),'id');//获取所有选中的id
            $groups=$this->group->select();
            foreach ($groups as &$g){
                if(in_array($g->id,$group_checked_id)){//判断当前是否选中
                    $g->checked='checked';
                }else{
                    $g->checked='';
                }
            }
            View::assign(compact('user','groups'));
            return View::fetch();
        }
    }

    /**
     * 修改密码
     * @param Request $request
     * @param $id
     * @return Json
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     */
    public function password(Request $request, $id)
    {
        if ($request->isPost()) {
            $data = $request->post();
            if (!$this->v->scene('password')->check($data)) {
                return json(['code' => 404, 'msg' => $this->v->getError(), 'data' => null]);
            }
            $user = Users::find($id);
            $result = $user->save($data);
            if ($result) {
                return json(['code' => 0, 'msg' => '成功', 'data' => null]);
            } else {
                return json(['code' => 0, 'msg' => '失败','data'=> null]);
            }
        }
    }

    public function authUser(Request $request, $id){
        if ($request->isPost()) {
            $data = $request->post();
            $user=$this->m->find($id);
            //先删除
            $user->userGroup()->detach();
            //再添加
            $user->userGroup()->saveAll($data['groups']);
           return xjson('授权成功！');
        }
        $user=$this->m->with('userGroup')->find($id);
        $group_checked_id=array_column($user->userGroup->toArray(),'id');//获取所有选中的id
        $groups=$this->group->select();
        foreach ($groups as &$g){
            if(in_array($g->id,$group_checked_id)){//判断当前是否选中
                $g->checked='checked';
            }else{
                $g->checked='';
            }
        }
        View::assign(compact('user','groups'));
        return View::fetch();
    }

}