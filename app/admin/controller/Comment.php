<?php

namespace app\admin\controller;

use app\admin\model\Comment as Comments;
use app\admin\traits\ControllerTrait;

class Comment extends AdminBase
{
    use ControllerTrait;
    public function initialize()
    {
        parent::initialize();
        $this->m = new Comments;   //别名：避免与控制名冲突
    }

    public function tree($page = 1, $limit = 10)
    {
        $count=$this->m->count();
        $data=$this->m->with('news')->order('id','desc')->page($page,$limit)->select();
        return json([
            'code'=>0,
            'msg'=>'查询成功',
            'count'=>$count,
            'data'=>$data
        ]);
    }

}