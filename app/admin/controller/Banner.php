<?php

namespace app\admin\controller;

use app\admin\model\Banner as Banners;
use app\admin\traits\ControllerTrait;
use app\admin\validate\Banner as BannerValidate;

class Banner extends AdminBase
{
    use ControllerTrait;
    public function initialize()
    {
        parent::initialize();
        $this->m = new Banners;   //别名：避免与控制名冲突
        $this->v=new BannerValidate;   //创建一个验证类
    }

    public function tree($page = 1, $limit = 10)
    {
        $count=$this->m->count();
        $data=$this->m->order('type,id desc')->page($page,$limit)->select();
        return json([
            'code'=>0,
            'msg'=>'查询成功',
            'count'=>$count,
            'data'=>$data
        ]);
    }
}