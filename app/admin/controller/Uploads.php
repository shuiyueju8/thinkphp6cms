<?php

namespace app\admin\controller;

use app\BaseController;
use app\Request;
use app\common\upload\Upload;

/*
 * 共用文件上传接口
 */

class Uploads extends BaseController
{
    protected $up;

    public function initialize()
    {
        parent::initialize();
        $this->up = conf('up');
    }

    public function index()
    {
        $data = $this->request->param();
        if (isset($data['action']) && $data['action']) {
            call_user_func($data['action']);
        }
    }

    /**
     * 上传文件
     */
    public function upload()
    {
        $files = request()->file();
        if ($files) {
            $res = Upload::upload('封面图');
            if ($res['code'] === 0) {
                return json(['state' => 'SUCCESS', 'url' => $res['url'], 'title' => $res['originalName']]);
            } else {
                return json(['state' => 'EROOR', 'title' => '图片上传成功上传失败!']);
            }
        } else {
            return json(['state' => 'EROOR', 'title' => '上传图片为空！']);
        }
    }

    /*
     * 上传文件，markdown上传接口
     */
    public function upload_img_markdown(Request $request)
    {
        $files = $request->file();
        if ($files) {
            if ($files) {
                $res = Upload::upload('markdown文章图');
                if ($res['code'] === 0) {
                    return json(['success' => 1, 'message' => "上传成功", 'url' => $res['url']]);
                } else {
                    return json(['success' => 0, 'message' => "本地上传失败!"]);
                }
            } else {
                return json(['success' => 0, 'title' => '上传图片为空！']);
            }
        }
    }
    /**
     * layui图片通用上传接口
     * @return \think\Response
     */
    /*
    public function img()
    {
        $files = request()->file();
        $dir = request()->get('dir');
        $dir = $dir ? $dir : 'default';
        $upload_dir = '/uploads/' . $dir ;
        if ($files) {
            $file = array_shift($files);
            // 移动到框架应用根目录/uploads/ 目录下
            $info = $file->validate(['size' => $this->up['image_size'], 'ext' => $this->up['image_ext']])->move('.' . $upload_dir);
            if ($info) {
                $up=$this->up;
                $pathName = $info->getPathName();//上传的图片路径
                $name=$info->getInfo()['name'];//上传后的图片名字
                $thumbPathName = str_replace($info->getFileName(), 'thumb-' . $info->getFileName(), $pathName);//处理后图片保存路径
                if($up['image_zip_status']||$up['watermark_status']){
                    $image = Image::open($pathName);
                    if ($up['image_zip_status']) {//启用图片压缩
                        //压缩图片大小
                        $yt_width = $image->width();
                        $yt_height = $image->height();
                        if ($yt_width > $up['image_zip_width']||$yt_height>$up['image_zip_height']||$up['image_zip_width']>0||$up['image_zip_height']>0) {//超过定义的宽度，并且不为零启用压缩
                            $height = $image->height() / $yt_width * $up['image_zip_size'];
                            $width = $up['image_zip_size'];
                            if($up['image_zip_width']>0){
                                $width=$up['image_zip_width'];
                                if($up['image_zip_height']>0){
                                    $height=$up['image_zip_height'];
                                }else{
                                    $height = $image->height() / $yt_width * $up['image_zip_size'];
                                }
                            }else{
                                if($up['image_zip_height']>0){
                                    $height=$up['image_zip_height'];
                                    $width= $image->width() / $yt_height * $up['image_zip_height'];
                                }else{//设置的宽高都为零，不进行剪裁

                                }
                            }
                            $image->crop($image->width(), $image->height(), 0, 0, $width, $height);//缩小图片
                        }
                    }
                    if ($up['watermark_status']) {//添加水印
                        try {
                            $image_water = Image::open('.' . $up['watermark_url']);//打开水印图
                            $width = $yt_width * 0.17;
                            $height = $image_water->height() / $image_water->width() * $width;
                            $image_water_tmp = '.' . $up['watermark_url'] . uniqid() . '.temp';//生成临时的水印图片
                            $image_water->crop($image_water->width(), $image_water->height(), 0, 0, $width, $height)->save($image_water_tmp, null, 100);//生成适合当前图片大小的水印图片大小
                            unset($image_water);//销毁水印对象
                            $image->water($image_water_tmp, Image::WATER_SOUTHEAST, 100);
                            unlink($image_water_tmp);//删除临时水印图片
                        } catch (\Exception $exception) {
                            //水印图片不存在则不进行添加水印操作
                        }
                    }
                    $image->save($thumbPathName, null, $up['image_zip_quality']);//处理图片
                    unset($image);//销毁对象
                    $src = substr($thumbPathName, 1);
                    if (!$up['image_original_status']) {//不保留原图
                        unset($info);//必须先解除锁定
                        unlink($pathName);//删除原图
                    }
                }else{//原图返回
                    $src = substr($pathName, 1);
                }
                $src=str_replace('\\', '/', $src);//替换'\'为'/'
                return xjson(['src' => $src, 'url' => $src, 'title' => $name]);
            } else {
                return xjson(500, $file->getError());
            }
        } else {
            return xjson(404, '上传的图片不能为空！');
        }
    }
    */
}
