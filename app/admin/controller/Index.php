<?php
namespace app\admin\controller;

use think\facade\App;
use app\admin\model\AuthRule;
use think\Controller;
use think\facade\Session;
use think\facade\Db;
use app\admin\model\user as userModel;
use think\facade\Cache;
use think\facade\view;

class Index extends AdminBase
{
    public function index()
    {
        $user=\app\admin\model\User::with('userGroup')->find(session('uid'));
        $rules=[];
        foreach ($user['userGroup'] as $group){
            $rules=array_merge($rules,explode(',',$group['rules']));
        }
        $menus= \app\admin\model\AuthRule::getMenu($rules);
        return view('index', ['menus' => $menus]);
    }

    public function welcome()
    {
        // return view();
        // $u=\think\facade\Cache::get('myConfig');
        // halt($u);
        //获取用户信息
        $user_id=session('uid');
        if($user_id){
            $userM = new userModel();
            $user=$userM->where(['id'=>$user_id])->field('id,username,name')->find();
        }       
        view::assign('user',$user);
        //获取服务器信息
        $mysqlVersion = Db::query('SELECT VERSION() AS ver');

        $info=[
        'url'             => $_SERVER['HTTP_HOST'],
        'domain_ip'=>$_SERVER['SERVER_NAME'].' [ '.gethostbyname($_SERVER['SERVER_NAME']).' ]',//服务器域名/IP

        'server_soft'     => $_SERVER["SERVER_SOFTWARE"],//运行环境
        'PHP_run'         => php_sapi_name(),//PHP运行方式
        
        'php_version'     => PHP_VERSION,//php版本
        'document_root'   => $_SERVER['DOCUMENT_ROOT'],
        'server_os'       => PHP_OS,//操作系统
        'server_port'     => $_SERVER['SERVER_PORT'],
        'server_ip'       => isset($_SERVER['SERVER_ADDR']) ? $_SERVER['SERVER_ADDR'] : '',
        'Think_version'   => App::version().' [ <a href="http://thinkphp.cn" target="_blank">查看最新版本</a> ]',//ThinkPHP版本
        'mysql_version'   => $mysqlVersion[0]['ver'],
        'max_upload_size' => ini_get('upload_max_filesize'),//上传附件限制
       // 'app_version'    => Config::get('app._version'),

        'max_execution_time'=>ini_get('max_execution_time').'秒',//执行时间限制
        'server_time'=>date("Y年n月j日 H:i:s"),//服务器时间
        'server_time_beijing'=>gmdate("Y年n月j日 H:i:s",time()+8*3600),//北京时间
        
        'disk_free_space'=>round((disk_free_space(".")/(1024*1024)),2).'M',//剩余空间
        'register_globals'=>get_cfg_var("register_globals")=="1" ? "ON" : "OFF",
        'magic_quotes_gpc'=>(1===get_magic_quotes_gpc())?'YES':'NO',
        'magic_quotes_runtime'=>(1===get_magic_quotes_runtime())?'YES':'NO',
        ];

        view::assign('serverinfo',$info);
        return view::fetch();

    }
    public function index2()
    {
        $a=AuthRule::all();
        dump($a);exit;
    }

}
