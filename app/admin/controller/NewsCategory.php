<?php
namespace app\admin\controller;

use app\news\model\NewsCategory as NewsCategorys;
use app\admin\traits\ControllerTrait;
use app\admin\validate\NewsCategory as cValidate;
use think\Request;
use think\facade\View;

class NewsCategory extends AdminBase
{
    use ControllerTrait;
    public function initialize()
    {
        parent::initialize();
        $this->m = new NewsCategorys;   //别名：避免与控制名冲突
        $this->v=new cValidate;   //创建一个验证类
    }
    //获取所有节点树
    public function tree2()
    {
        $dataList = $this->m->treeList2();
        // dump($this->m);
        // dump($dataList);die;
        if (count($dataList)){
            return json(['msg'=>'success','code'=>0,'data'=>$dataList,'count'=>count($dataList),'is'=>true,'tip'=>'操作成功']);
        }else{
            return json(['msg'=>'暂无数据','code'=>0,'data'=>[],'count'=>0]);
        }
    }

    //添加时调用
    public function tree()
    {
        return json($this->m->tree());
    }

    /**
     * 保存新建的资源
     *
     * @param \think\Request $request
     * @return \think\Response
     * @throws \Exception
     */
    public function add()
    {
        if (request()->isPost()) {
            $data = input('post.');
            if ($this->v && !$this->v->scene('add')->check($data)) {
                return xjson(404, $this->v->getError());
            }
            $result = $this->m->create($data);
            if ($result) {
                return xjson();
            } else {
                return xjson(404, '添加失败！');
            }
        }
        $treeList = $this->m->tree();
        View::assign('treeList', $treeList);
        return View::fetch();
    }

    /**
     * 复制当前节点并编辑
     *
     * @param  \think\Request  $request
     * @return \think\Response
     */
    public function copy(Request $request)
    {
        if ($request->isPost()){
            $data = input('post.');
            if (! $this->v->scene('add')->check($data)) {
                $this->result(null,400,$this->v->getError());
            }
            $result = $this->m->create($data);
            if ($result){
                return json(['code'=>0,'msg'=>'成功']);
            }else{
                return json(['code'=>400,'msg'=>'保存失败']);
            }
        }
        $data=$this->m->find($request->get('id'));
        $treeList = $this->m->tree();
        return view('copy',compact('data','treeList'));
    }

    public function edit($id)
    {
        if ($this->request->isPost()) {
            $data = $this->request->post();
            if ($this->v && !$this->v->scene('edit')->check($data)) {
                return xjson(404, $this->v->getError());
            }
            $this->m = $this->m->find($id);
            $result = $this->m->save($data);
            if ($result) {
                return xjson();
            } else {
                return xjson(404, '修改失败!');
            }
        } else {
            $data = $this->m->find($id);
            $treeList = $this->m->tree();
            View::assign(compact('treeList','data'));
            return View::fetch();
        }
    }

    public function delete($id)
    {
        if (request()->isPost()) {
            if(is_array($id)){//批量删除

            }else{//单选删除
                $list=$this->m->tree($id);//获取该节点下所有节点数组
                $id=[$id];
                if($list){//存在子节点则合并
                    return xjson(404,'存在子分类，请先删除子分类再执行删除。');
                    $id=array_merge($id,array_column($list,'id'));
                }
            }
            //级联删除该id下的所有节点
            $result = NewsCategorys::destroy($id);
            if ($result) {
                return json(['code' => 0, 'msg' => '成功删除'.count($id).'条记录', 'data' => null]);
            } else {
                return json(['code' => 404, 'msg' => '删除失败', 'data' => null]);
            }
        }
    }

}