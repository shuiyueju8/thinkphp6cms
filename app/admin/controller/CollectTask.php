<?php
namespace app\admin\controller;

use think\facade\View;
use think\Request;
use think\response\Json;
use function GuzzleHttp\Psr7\str;

class CollectTask extends AdminBase
{
    public function initialize()
    {
        parent::initialize();
        $this->m = new \app\admin\model\collect\CollectTask();   //别名：避免与控制名冲突
        $this->v = new \app\admin\validate\collect\CollectTask();   //别名：避免与控制名冲突
    }

    public function index()
    {
        return View::fetch("collect/collect_task/index");
    }

    public function tree($page = 1, $limit = 10)
    {
        return $this->m->pageTable($page, $limit);
    }

    public function add()
    {
        $collectRules=\app\admin\model\collect\CollectRule::select();
        $collectTasks=$this->m->where("status",SUCCESS)->select();
        return View::fetch("collect/collect_task/add",compact("collectRules",'collectTasks'));
    }

    public function add_type1(){
        if (request()->isPost()) {
            $data = input('post.');
            if ($this->v && !$this->v->scene('add_type1')->check($data)) {
                return xjson(404, $this->v->getError());
            }
            $urls=explode("\n",$data['urls']);
            $data['matching']=$urls[0];
            $result = $this->m->create($data);
            if ($result) {
                return xjson();
            } else {
                return xjson(404, '添加失败！');
            }
        }
    }

    public function add_type2(){
        if (request()->isPost()) {
            $data = input('post.');
            if ($this->v && !$this->v->scene('add_type2')->check($data)) {
                return xjson(404, $this->v->getError());
            }
            if($data['matching_end']-$data['matching_start']>10000){
                return xjson(404, "采集页面数量过多...超过1万了");
            }
            $result = $this->m->create($data);
            if ($result) {
                return xjson();
            } else {
                return xjson(404, '添加失败！');
            }
        }
    }
    public function add_type3(){
        if (request()->isPost()) {
            $data = input('post.');
            if ($this->v && !$this->v->scene('add_type3')->check($data)) {
                return xjson(404, $this->v->getError());
            }
            $result = $this->m->create($data);
            if ($result) {
                return xjson();
            } else {
                return xjson(404, '添加失败！');
            }
        }
    }

    public function edit_type1($id){
        $data = input('post.');
        if ($this->v && !$this->v->scene('editType1')->check($data)) {
            return xjson(404, $this->v->getError());
        }
        $result = $this->m->find($id);
        $urls=explode("\n",$data['urls']);
        $data['matching']=$urls[0];
        $result=$result->save($data);
        if ($result) {
            return xjson();
        } else {
            return xjson(404, '更新成功失败！');
        }
    }
    public function edit_type2($id){
        $data = input('post.');
        if ($this->v && !$this->v->scene('editType2')->check($data)) {
            return xjson(404, $this->v->getError());
        }
        $result = $this->m->find($id);
        $result=$result->save($data);
        if ($result) {
            return xjson();
        } else {
            return xjson(404, '更新成功失败！');
        }
    }
    public function edit_type3($id){
        $data = input('post.');
        if ($this->v && !$this->v->scene('editType3')->check($data)) {
            return xjson(404, $this->v->getError());
        }
        $result = $this->m->find($id);
        $result=$result->save($data);
        if ($result) {
            return xjson();
        } else {
            return xjson(404, '更新成功失败！');
        }
    }

    public function edit($id)
    {
        $data = $this->m->find($id);
        $collectRules=\app\admin\model\collect\CollectRule::select();
        $collectTasks=$this->m->where("status",SUCCESS)->select();
        return View::fetch("collect/collect_task/edit_type{$data['type']}",compact('data','collectRules','collectTasks'));
    }

    public function ajax($id)
    {
        if ($this->request->isPost()) {
            $data = $this->request->post();
            if ($this->v && !$this->v->scene('ajax')->check($data)) {
                return json(['code' => 404, 'msg' => $this->v->getError(), 'data' => null]);
            }
            $m = $this->m->find($id);
            $m[key($data)] = $m[key($data)] ? 0 : 1;
            $result = $m->save();
            if ($result) {
                return xjson();
            } else {
                return xjson(404, '修改失败！');
            }
        } else {
            return xjson(403, '请求不被允许');
        }
    }

    public function delete($id)
    {
        if (request()->isPost()) {
            $result = $this->m->where('id','in',$id)->delete();
            if ($result) {
                return xjson( '删除成功！');
            } else {
                return xjson(404, '删除失败！');
            }
        }
    }
    public function reset($id)
    {
        if (request()->isPost()) {
            $result = $this->m->where('id',$id)->update(["status"=>READY]);
            if ($result) {
                return xjson( '设置成功！');
            } else {
                return xjson(404, '设置失败！');
            }
        }
    }
}