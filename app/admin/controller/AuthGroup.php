<?php

namespace app\admin\controller;

use app\admin\model\AuthGroup as AuthGroups;
use app\admin\validate\AuthGroup as AuthGroupsValidate;
use app\admin\traits\ControllerTrait;
class AuthGroup extends AdminBase
{
    use ControllerTrait;
    protected function initialize()
    {
        parent::initialize();
        $this->m = new AuthGroups;   //别名：避免与控制名冲突
        $this->v = new AuthGroupsValidate;   //别名：避免与控制名冲突
    }

    public function index()
    {
        $dataList = $this->m->paginate(10);
        return view('index',['dataList'=>$dataList]);
    }


    public function tree($page = 1, $limit = 10)
    {
        return $this->m->pageTable($page, $limit);
    }

    /*
     * 用户用户id获取选中的权限节点list
     */
    public function getUserRule($id)
    {
        $auth_group = $this->m->find($id);
        $rule = new \app\admin\model\AuthRule();
        return json($rule->getUserRule($auth_group['rules']));

    }
    /*
     * ajax更新部分数据
     */
    public function ajax($id)
    {
        if ($this->request->isPost()) {
            if($id==1){
                return xjson(404, '你无法禁用超级管理员');
            }
        }
        return parent::ajax($id);
    }

    public function delete($id)
    {
        if (request()->isPost()) {
            //超级管理员不能删除
            if($id==1||strpos($id,'1,'!==false)){
                return xjson(404, '超级管理员不能删除');
            }
            $result = $this->m->where('id','in',$id)->delete();
            if ($result) {
                return xjson();
            } else {
                return xjson( 404);
            }
        }
    }

}