<?php
namespace app\admin\model;

use think\facade\Db;

class Tags extends Common
{
    protected $autoWriteTimestamp = true;
    protected $insert = ['hot' => 0];
    public function news()
    {
        return $this->belongsToMany(News::class, NewsTags::class, 'news_id','tags_id');
    }
    public static function hot_tags($limit=15){
        
        $hot_tags=Db::query("SELECT tags.id,tags.name,COUNT(tags_id) num FROM news_tags LEFT JOIN tags ON news_tags.tags_id=tags.id GROUP BY news_tags.tags_id ORDER BY num desc LIMIT {$limit}");
        return $hot_tags;
    }
}