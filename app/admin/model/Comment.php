<?php
namespace app\admin\model;

class Comment extends Common
{
    //评论表
    protected $autoWriteTimestamp = true;
    //关联文章表
    public function news(){
       return $this->belongsTo(News::class);
    }
}