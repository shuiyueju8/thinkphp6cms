<?php
namespace app\admin\model;

use app\admin\model\Common;

class User extends Common
{
    protected $readonly = ['username'];
    
    protected $insert  = ['logins', 'reg_ip', 'last_time', 'last_ip'];
    //protected $update = [];
    
    public function userInfo()
    {
        return $this->hasOne('userInfo', 'uid', 'id');
    }
    
    public function userGroup()
    {
        return $this->belongsToMany('authGroup', 'auth_group_access', 'group_id','uid');
    }
    
    protected function setPasswordAttr($value)
    {
        return md5($value);
    }
    protected function setLoginsAttr()
    {
        return '0';
    }
    protected function setRegIpAttr()
    {
        return request()->ip();
    }
    protected function setLastTimeAttr()
    {
        return time();
    }
    protected function setLastIpAttr()
    {
        return request()->ip();
    }
    
    public function getStatusTurnAttr($value, $data)
    {
        $turnArr = [0=>lang('status0'), 1=>lang('status1')];
        return $turnArr[$data['status']];
    }
    public function getLastTimeTurnAttr($value, $data)
    {
        return date('Y-m-d H:i:s', $data['last_time']);
    }
}