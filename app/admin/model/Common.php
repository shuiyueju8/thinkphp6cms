<?php
namespace app\admin\model;

use think\Model;

class Common extends Model
{
    /**
     * 直接返回layUI分页表格所需是数据
     *
     * @param int $page 页码
     * @param int $limit 每页显示条数
     * @return \think\response\Json
     */
    public function pageTable($page=1,$limit=10)
    {
        return json([
            'code'=>0,
            'msg'=>'查询成功',
            'count'=>self::count(),
            'data'=>self::page($page,$limit)->select()
        ]);
    }


    /**
     * 插入全部,数据库有记录则不插入,直接返回改记录
     * @param $date
     * @return array
     */
    public static function uniqueSave($data){
        $collection=[];
        if (is_array($data)) {
            if (key($data) === 0) {
                foreach ($data as $d){
                    $t= self::where($d)->find();
                    if(!$t){
                        $t=self::create($d);
                    }
                    $collection[]=$t->toArray();
                }
            } else {
                $t= Tags::where($data)->find();
                if(!$t){
                    $t=Tags::create($data);
                }
                $collection=$t->toArray();
            }
        }
        return $collection;
    }
}