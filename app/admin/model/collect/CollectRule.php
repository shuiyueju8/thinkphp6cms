<?php

namespace app\admin\model\collect;

use app\admin\model\Common;

class CollectRule extends Common
{
    protected $autoWriteTimestamp = "datetime";

    public function setRulesAttr($rules)
    {
        return json_encode($rules);
    }

}