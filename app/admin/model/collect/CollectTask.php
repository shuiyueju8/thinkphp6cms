<?php


namespace app\admin\model\collect;


use app\admin\model\Common;

class CollectTask extends Common
{
    protected $autoWriteTimestamp = "datetime";
    public function setUrlsAttr($urls)
    {
        if($urls){
            return str_replace([",","，","|"],"\n",$urls);
        }else{
            return null;
        }
    }
}