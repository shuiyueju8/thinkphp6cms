<?php
namespace app\admin\validate;

use think\Validate;

class News extends Validate
{
    protected $rule = [
        'news_category_id|资讯分类' => 'require|integer',
        'title|资讯标题' => 'require|max:255',
        'abstract|资讯摘要' => 'require|max:500',
        'content|资讯内容' => 'require',
        'top|置顶' => 'in:0,1',
        'recommend|推荐' => 'in:0,1',
        'hot|热门' => 'in:0,1',
        'browse|浏览量' => 'integer',
        'cover|封面图' => 'require|max:255',
    ];
    protected $scene = [
        'add'   => ['news_category_id','title','abstract', 'content','top','recommend','hot','browse','cover'],
        'edit'  =>  ['news_category_id','title','abstract', 'content','top','recommend','hot','browse','cover'],
    ];
    // ajax 验证场景定义
    public function sceneAjax()
    {
        return $this->only(['top',  'recommend','hot']);
    }
}