<?php
namespace app\admin\validate;

use think\Validate;

class Blogroll extends Validate
{
    protected $rule = [
        'name|网站名称' => 'require|max:20',
        'blogroll|友情链接' => 'require|max:50',
        'sorts|排序' => 'require|between:1,999',
        'status|是否显示' => 'require|in:0,1',
    ];
    protected $scene = [
        'add'   => ['name','blogroll','sorts'],
        'edit'  => ['name','blogroll','sorts'],
        'ajax'  => ['status'],
    ];
}