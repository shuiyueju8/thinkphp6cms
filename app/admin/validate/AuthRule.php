<?php
namespace app\admin\validate;

use think\Validate;

class AuthRule extends Validate
{
    protected $rule = [
        'pid|父级分类' => 'require|integer',
        'title|节点名称' => 'require',
        'name|节点地址' => 'require|/^[a-zA-Z0-9\/\-\_]+$/',
        'status' => 'require|in:0,1',
        'ismenu' => 'require|in:0,1',
        'sorts' => 'require|integer|>=:1',
        'node|默认节点'=>'require|array'
    ];

    protected $scene = [
        'add'   => ['pid', 'title', 'name', 'status', 'ismenu', 'sorts'],
        'status' => ['status'],
        'ismenu' => ['ismenu'],
        'title' => ['title'],
        'name' => ['name'],
        'resource'=>['name','title','node']
    ];
    // edit 验证场景定义
    public function sceneEdit()
    {
        return $this->only(['pid', 'title', 'name', 'status', 'ismenu', 'sorts'])
            ->remove('name', 'unique');
    }

    // ajax 验证场景定义
    public function sceneAjax()
    {
        return $this->only(['status',  'ismenu'])
            ->remove('status', 'require')
            ->remove('ismenu', 'require');
    }
}