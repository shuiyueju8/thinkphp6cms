<?php
namespace app\admin\validate;

use think\Validate;

class User extends Validate
{
    protected $rule = [
        'username|用户名' => 'require|min:1|unique:user',
        'password|密码' => 'require|min:6',
        'rpassword|重复密码' => 'require|confirm:password',
        'email|邮箱' => 'email|unique:user',
        'mobile|手机号' => '/^1[34578]\d{9}$/|unique:user',
        'sex|性别' => 'require|in:0,1,2',
        'status|状态' => 'require|in:0,1',
    ];

    protected $scene = [
        'add'   => ['username', 'password', 'rpassword', 'email', 'mobile', 'sex', 'status'],
        'edit'  => ['email', 'mobile', 'sex', 'status'],
        'password' => ['password', 'rpassword'],
    ];
    // edit 验证场景定义
    public function sceneEdit()
    {
        return $this->only(['email', 'mobile', 'sex', 'status'])
            ->remove('email', 'unique')
            ->remove('mobile', 'unique');
    }

    // ajax 验证场景定义
    public function sceneAjax()
    {
        return $this->only(['status'])
            ->remove('status', 'require');
    }
}