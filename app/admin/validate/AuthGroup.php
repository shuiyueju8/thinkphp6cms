<?php
namespace app\admin\validate;

use think\Validate;

class AuthGroup extends Validate
{
    protected $rule = [
        'title|角色名称' => 'require',
        'status|状态' => 'require|in:0,1',
        'level|排序' => 'require|between:0,9999',
        'rules|权限' => 'require',
    ];
    protected $scene = [
        'add'   => ['title',  'status', 'rules','level'],
        'edit'  => ['title', 'status', 'rules','level'],
    ];
    // ajax 验证场景定义
    public function sceneAjax()
    {
        return $this->only(['title',  'status', 'rules','level'])
            ->remove('title', 'require')
            ->remove('status', 'require')
            ->remove('rules', 'require')
            ->remove('level', 'require');
    }
}