<?php
namespace app\admin\validate;

use think\Validate;

class Banner extends Validate
{
    protected $rule = [
        'cover|封面图' => 'require|max:255',
        'title|资讯标题' => 'require|max:255',
        'sorts|排序' => 'require|between:1,10',
    ];
    protected $scene = [
        'add'   => ['cover','title','sorts'],
        'edit'  =>  ['cover','title','sorts'],
    ];
}