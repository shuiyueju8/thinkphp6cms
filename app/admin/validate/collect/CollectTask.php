<?php


namespace app\admin\validate\collect;


use think\Validate;

class CollectTask extends Validate
{
    protected $rule = [
        'name|采集任务名' => 'require|unique:collect_task|max:255',
        'urls|采集任务列表' => 'require',
        'collect_rule_id|采集规则' => 'require|integer',
        'matching|匹配链接' => 'require|max:255',
        'matching_start|开始值' => 'require|integer',
        'matching_end|结束值' => 'require|integer'
    ];
    protected $scene = [
        'add_type1'   => ['name','urls','collect_rule_id'],
        'add_type2'   => ['name','collect_rule_id','matching','matching_start','matching_end'],
        'add_type3'   => ['name','collect_rule_id','matching','matching_start'],
        'edit'   => ['name','collect_rule_id'],
    ];

    public function sceneEditType1()
    {
        return $this->only(['name',  'urls','collect_rule_id'])
            ->remove('name',  'unique|max:255');
    }
    public function sceneEditType2()
    {
        return $this->only(['name','collect_rule_id','matching','matching_start','matching_end'])
            ->remove('name',  'unique|max:255');
    }
    public function sceneEditType3()
    {
        return $this->only(['name','collect_rule_id','matching','matching_start'])
            ->remove('name',  'unique|max:255');
    }
}