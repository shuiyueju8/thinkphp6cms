<?php
namespace app\admin\validate\collect;

use think\Validate;

class CollectRule extends Validate
{
    protected $rule = [
        'name|采集名称' => 'require|max:255',
        'url|资讯标题' => 'require|url|max:255',
        'type|采集类型' => 'in:1,2',
        'rules|采集规则' => 'require|array',
        'range|分片选择器' => 'require|max:255',
        'take|采集数量' => 'integer|<=:9999',
        'remove|移除选择器' => 'max:255',
        'sorts' => 'require|integer|>=:1',
    ];
    protected $scene = [
        'add_type1'   => ['name','url','rules','remove', 'sorts','type'],
        'add_type2'   => ['name','url', 'type','rules','take','remove','sorts'],
    ];

}