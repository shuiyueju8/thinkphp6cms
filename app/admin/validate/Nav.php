<?php
namespace app\admin\validate;

use think\Validate;

class Nav extends Validate
{
    protected $rule = [
        'pid|父级导航' => 'require|integer',
        'title|导航名字' => 'require',
        'status|状态' => 'require|in:0,1',
        'sorts|排序' => 'require|integer|>=:1',
    ];

    protected $scene = [
        'add'   => ['pid',  'title', 'status', 'sorts'],
        'status' => ['status'],
        'title' => ['title'],
    ];
    // edit 验证场景定义
    public function sceneEdit()
    {
        return $this->only(['pid',  'title', 'status', 'sorts'])
            ->remove('title', 'unique');
    }

    // ajax 验证场景定义
    public function sceneAjax()
    {
        return $this->only(['status',  'ismenu'])
            ->remove('status', 'require')
            ->remove('ismenu', 'require');
    }
}