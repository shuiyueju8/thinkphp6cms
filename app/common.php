<?php

const RULE_TYPE_SIMPLE=1;
const RULE_TYPE_RANGE=2;
const ESTABLISH=0;//采集任务建立
const READY=10;//采集任务待执行
const RUNNING=20;//采集任务执行中
const SUCCESS=30;//采集任务全部执行成功
const PART_SUCCESS=31;//采集任务部分执行成功
const ERROR=40;//采集任务执行失败
const TIME_OUT=50;//任务超时
const TASK_TYPE_SIMPLE=1;//简单采集任务
const TASK_TYPE_INCREASING=2;//自增采集任务
const TASK_TYPE_AGAIN=3;//任务再采集采集任务


// 应用公共文件
//共用返回json函数
function xjson($code=0,$msg='操作成功',$data=[]){
    $json=['code'=>0,'msg'=>$msg,'data'=>$data];
    $num= func_num_args();
    for ($i=0;$i<$num;$i++){
        $value=func_get_arg($i);
        if(is_numeric($value)){
            $json['code']=$value;
            if($value){
                $json['msg']='操作失败！';
            }else{
                $json['msg']='操作成功！';
            }
        }elseif(is_string($value)){
            $json['msg']=$value;
        }else{
            $json['data']=$value;
        }
    }
    return \think\Response::create($json, 'json',200);
}
/**
 * 无限级分类，不带层级
 * @param $data '所有分类数据
 * @param int $pid
 * @param string $title 给数据中哪个字段加上-“---”分隔符，区别层级
 * @param int $level
 * @param string $field
 * @param Closure|null $f
 * @return array
 */
function unlimitedSort($data,$pid = 0, $level=1,$field = 'pid',$title='title',Closure $f=null)
{
    if(count($data)==0){
      return [];
    }
    if(isset($data[0])&&$data[0] instanceof \think\Model ){
        $data= $data->toArray();
    }
    static $tree = array();
    foreach ($data as $item) {
        if ($item[$field] == $pid) {
            $item['level'] = $level;
            $item['pid']=(int)$item['pid'];
            if(isset($f)){
                $f($item);
            }
            $item['_title']=str_repeat('　　',$level-1).'├'.$item[$title];
            $tree[] = $item;
            unlimitedSort($data,$item['id'],$level+1,$field,$title,$f);
        }
    }
    return $tree;
}


/**
 * 返回带children层级的无限级分类
 * @param $data
 * @param int $pid
 * @param int $level
 * @param string $field
 * @param string $childNode
 * @param Closure|null $f
 * @return array
 */
function unlimitedTree($data,$pid = 0, $level=1,$field = 'pid', $childNode = 'children',Closure $f=null)
{
    if(count($data)==0){
        return [];
    }
    if(isset($data[0])&&$data[0] instanceof \think\Model ){
        $data= $data->toArray();
    }
    $tree = [];
    foreach ($data as $item) {
        if ($item[$field] == $pid) {
            $item[$childNode] = unlimitedTree($data,$item['id'],$level+1,$field,$childNode,$f);
            $item['level'] = $level;
            $item['pid']=(int)$item['pid'];
            if(isset($f)){
                $f($item);
            }
            $tree[] = $item;
        }
    }
    return $tree;
}

/**
 * 获得所有父级栏目
 * @param $data 栏目数据
 * @param $id 子栏目
 * @param string $fieldPri 唯一键名，如果是表则是表的主键
 * @param string $fieldPid 父ID键名
 * @return array
 */
function parentNode($data, $id, $fieldPri = 'id', $fieldPid = 'pid')
{
    if(!empty($data)&&$data[0] instanceof \think\Model ){
        $data= $data->toArray();
    }
    $arr = array();
    foreach ($data as $v) {
        if ($v[$fieldPri] == $id) {
            $arr[] = $v;
            $_n =parentNode($data, $v[$fieldPid], $fieldPri, $fieldPid);
            if (!empty($_n)) {
                $arr = array_merge($arr, $_n);
            }
        }
    }
    return $arr;
}
/**
 * 获取后端configs表参数，并存入缓存中，若参数不存在则直接返null
 * @param mixed ...$params 可变参数
 * @return mixed|null 返回配置项数组或者值
 */
function conf($type=null,$value=null){
    $myConfig = \think\facade\Cache::remember('myConfig', function(){
        $all=\think\facade\Db::name('configs')->select();
        $format_data=[];
        foreach ($all as $d){
            $format_data[$d['type']][$d['key']]=$d['value'];//格式化数据
        }
        return $format_data;
    });
    $res=$myConfig;
    $res=isset($value)?$res[$type][$value]:(isset($type)?$res[$type]:$res);
    return $res;
}