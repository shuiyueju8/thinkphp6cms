<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------
use think\facade\Route;

 //article
Route::get('article/index','article/index');
#Route::get('article/:id','Article/index')->name('article/index')->ext('html');

Route::get('article/:id','article/index')->pattern(['id'=>'\d+'])->ext('html');
//个人博客日记
//Route::get('/:page', 'Category/index')->append(['id' => 1])->name('category.blog')->ext('html');
Route::rule('category/index','category/index');
Route::rule('category/:id','category/index')->pattern(['id'=>'\d+']);
Route::rule('category/:id','category/index')->pattern(['id'=>'\d+'])->ext('html');
//技术交流
#Route::get('blog/technology/:page', 'blog/Category/index')->append(['id' => 2])->ext('html');


 //文章标签路由
 Route::get('tag/:name', 'Tag/index')->name('tag')->ext('html');
 Route::get('tag/:name/:page', 'Tag/index')->name('tag.index')->ext('html');
 //文章标签路由
 Route::get('/search/:page', 'Article/search')->name('article.search')->ext('html');