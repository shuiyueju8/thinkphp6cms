<?php
declare (strict_types=1);

namespace app\news\controller;

use app\admin\model\Banner;
use app\news\model\News;
use app\news\Sidebar;


class Index extends IndexBase
{
    public function index()
    {
        $baners_left = Banner::where('type', '首页左侧')->limit(4)->select();
        $baners_right = Banner::where('type', '首页右侧')->limit(2)->select();
        $baners_ad = Banner::where('type', '首页中部')->find();
        $baners_ad2 = Banner::where('type', '右侧中部')->select();
        //查询四篇置顶的文章
        $recent_top_news = News::where('top', '1')->with('newsCategory')->limit(4)->select();
        //查询十篇最新文章
        $recent_not_top_news = News::where('top', '0')->with('newsCategory')->order('create_time', 'desc')->limit(10)->select();
        //查询六篇推荐文章
        $recent_recommend_news = News::where('recommend', '1')->order('create_time', 'desc')->limit(6)->select();
        //查询个人博客热门文章两篇
        $recent_hot_news = News::where('hot', '1')->order('id', 'desc')->limit(2)->select();
        $recent_blog_news = News::where('hot', '0')->order('id', 'desc')->limit(5)->select();
        $sidebar=new Sidebar();
        $sidebar=$sidebar->add_sidebar([
            ['sidebar'=>'card'],
            ['sidebar'=>'recent_update'],
            ['sidebar'=>'recent_browse'],
            ['sidebar'=>'site_recommend'],
            ['sidebar'=>'ad','url'=>$baners_ad2[0]['url'],'cover'=>$baners_ad2[0]['cover']],
            ['sidebar'=>'think_you_like'],
            ['sidebar'=>'ad','url'=>$baners_ad2[1]['url'],'cover'=>$baners_ad2[1]['cover']],
            ['sidebar'=>'tongji_base'],
            ['sidebar'=>'blogroll'],
        ]);
        return view('index', compact('baners_left',
            'baners_right',
            'baners_ad',
            'baners_ad2',
            'recent_top_news',
            'recent_not_top_news',
            'recent_recommend_news',
            'recent_hot_news',
            'recent_blog_news',
            'sidebar'
        ));
    }
}
