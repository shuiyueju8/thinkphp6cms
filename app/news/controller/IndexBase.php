<?php
declare (strict_types = 1);

namespace app\news\controller;

use app\admin\model\Nav;
use app\BaseController;
use think\facade\View;

class IndexBase extends BaseController
{
    protected function initialize()
    {
        if (request()->isGet()){
            $nav=new Nav;
            $navs=$nav->getNavTree();
            $confs=conf();
            //dump($navs);
            View::assign(compact('navs','confs'));
        }

    }


}
