<?php
declare (strict_types=1);

namespace app\news\controller;
use app\admin\model\Banner;
use app\news\model\News;
use app\news\model\NewsTags;
use app\news\model\Tags;
use app\common\Bootstrap;
use app\news\Sidebar;
//use think\facade\Db;

class Tag extends IndexBase
{
    public function index()
    {
        $data=$this->request->param();
        $tag_name=$data['name'];
        $tag=Tags::where('name',$tag_name)->find();
        // dump($data);
        $news = News::hasWhere('newsTags',array('tags_id'=>$tag->id))->with('newsCategory')->paginate(10);
        $baners_ad2 = Banner::where('type', '右侧中部')->select();
        $sidebar=new Sidebar();
        $sidebar=$sidebar->add_sidebar([
            ['sidebar'=>'recent_update','total'=>4],
            ['sidebar'=>'site_recommend','total'=>4],
            ['sidebar'=>'ad','url'=>$baners_ad2[0]['url'],'cover'=>$baners_ad2[0]['cover']],
            ['sidebar'=>'think_you_like','total'=>4],
            ['sidebar'=>'ad','url'=>$baners_ad2[1]['url'],'cover'=>$baners_ad2[1]['cover']],
            ['sidebar'=>'hot_tags'],
            ['sidebar'=>'blogroll'],
        ]);
        return view('index',compact('news','tag','baners_ad2', 'sidebar'));
    }

//    public function index()
//    {
//        $data=$this->request->param();
//        $tag_name=$data['name'];
//        $page=(int)$data['page'];
//        $listRows=1;//每页显示数量
//        $tag=Tags::where('name',$tag_name)->with(array('news'=>function($query)use($page,$listRows){
//            $query->with('newsCategory')->page($page,$listRows);
//        }))->find();//模型关联查询数据
//        $list = Tags::where('name',$tag_name)->withCount('news')->find();
//        $results=$tag->news->toArray();//当前页数据结果集合
//        $total=$list->news_count;//关联查询统计数量
//        $news=new Bootstrap($results, $listRows, $page, $total,false,array('path'=>"/tag/{$tag_name}/1.html"));
//        $tag->news=$news;
//        return view('index',compact('tag'));
//
//
//        dump($news_page);exit();
//        $tags=Tags::where('name',$tag_name)->find();
////        dd($tags);exit();
//        echo Tags::paginate([
//            'list_rows'=> 1,
//            'page' => '1',
//        ])->render();exit();
//        $res=News::hasWhere('tags',array('name'=>$tag_name))->select();
//        var_dump($res);exit();
//
//        $id=1;
//        $news=News::where('news_category_id',$id)->with()
//            ->order('id','desc')
//            ->paginate(10);
//
//        //查询本栏目4篇推荐文章
//        $recommend_category_news = News::where('news_category_id',$id)->order(['recommend'=>'desc','id'=>'desc'])->limit(4)->select()->toArray();
//        $recommend_category_news_top =array_shift($recommend_category_news);
//        //查询本栏目阅读数量
//        $browse_category_news = News::where(['news_category_id'=>$id])->order(['browse'=>'desc','id'=>'desc'])->limit(5)->select()->toArray();
//        $browse_category_news_top= array_shift($browse_category_news);
//
//        //猜你6喜欢
//        $think_you_like_category_news = News::where(['news_category_id'=>$id])->orderRaw('rand()')->limit(6)->select();
//        return view('index',
//            compact('new_category',
//                'news',
//                'recommend_category_news',
//                'recommend_category_news_top',
//                'browse_category_news_top',
//                'browse_category_news',
//                'think_you_like_category_news')
//        );
//    }
}
