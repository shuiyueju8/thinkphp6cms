<?php

namespace app\news\model;

use Closure;
use think\Model;

class Nav extends Model
{
    protected $autoWriteTimestamp = true;
    public function tree($pid=0,$level=1,$field = 'pid',$title='title',Closure $f=null)
    {
        $list = $this->field('id,pid,title,url,status,sorts')->order('sorts ASC,id ASC')->select();
        return unlimitedSort($list,$pid, $level,$field,$title,$f);
    }
    public function tree2($id)
    {
        $list = \app\admin\model\AuthRule::select();
        return parentNode($list,$id);
    }
    public function getNavTree()
    {
        $list = $this->order('sorts ASC,id ASC')->select()->toarray();       
        return unlimitedTree($list);
    }

}