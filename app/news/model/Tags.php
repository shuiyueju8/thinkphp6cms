<?php
namespace app\news\model;

use think\facade\Db;
use think\facade\Config;

class Tags extends Common
{
    protected $autoWriteTimestamp = true;
    protected $insert = ['hot' => 0];
    public function news()
    {
        return $this->belongsToMany(News::class, NewsTags::class, 'news_id','tags_id');
    }
    public static function hot_tags($limit=15){
        $prefix=Config::get('database.prefix');

        // $hot_tags=Db::query("SELECT tags.id,tags.name,COUNT(tags_id) num FROM {$prefix}_news_tags LEFT JOIN {$prefix}_tags ON {$prefix}_news_tags.tags_id=tags.id GROUP BY {$prefix}_news_tags.tags_id ORDER BY num desc LIMIT {$limit}");
        $hot_tags=Db::name("tags")->field('id,name')->where(['hot'=>1])->limit(15)->select();
        return $hot_tags;
    }
}