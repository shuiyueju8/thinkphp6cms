<?php

namespace app\news\model;

use Closure;
use think\Model;

class NewsCategory extends Model
{
    protected $autoWriteTimestamp = true;
    public function tree($pid=0,$level=1,$field = 'pid',$title='title',Closure $f=null)
    {
        $list = $this->field('id,pid,title,status,sorts')->order('sorts ASC,id ASC')->select();
        return unlimitedSort($list,$pid, $level,$field,$title,$f);
    }

    public function treeList2()
    {
        $list = $this->order('sorts ASC,id ASC')->select();
        $list = unlimitedSort($list);
        // dump($list);
        return $list;
    }
}