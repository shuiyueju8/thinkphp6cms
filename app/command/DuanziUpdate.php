<?php
declare (strict_types = 1);

namespace app\command;

use app\admin\model\CoverPre;
use app\admin\model\JokeDetail;
use think\console\Command;
use think\console\Input;
use think\console\Output;
use think\facade\Db;

class DuanziUpdate extends Command
{
    protected function configure()
    {
        // 指令配置
        $this->setName('duanzi_update')
            ->setDescription('更新快乐段子');
    }

    protected function execute(Input $input, Output $output)
    {
        $duanziDetails=Db::table("duanzi")->where("status",2)->limit(3)->select();
        foreach ($duanziDetails as $detail){
            $title_array=explode('：',$detail['title']);
            $title=array_pop($title_array);
            $description=mb_substr(strip_tags($detail['content']),0,128);
            /********************************首部增加一张图********************************/
            $cover=CoverPre::where("type",8)->order("total,id")->find();
            $cover_gif=$cover->gif;
            $content="<p><img src=\"{$cover->gif}\" alt=\"{$cover->title}\"/></p>\n".$detail['content'];
            $cover->total=$cover->total+1;
            $cover->save();
            /********************************尾部增加一张图********************************/
            $cover=CoverPre::where("type",8)->order("total,id")->find();
            $content=$content."<p><img src=\"{$cover->gif}\" alt=\"{$cover->title}\"/></p>";
            $cover->total=$cover->total+1;
            $cover->save();
            $data=[
                "news_category_id"=>8,
                "cover"=>$cover_gif,
                "title"=>$title,
                "abstract"=>$description,
                "content"=>$content,
                "editor_type"=>0,
                "browse"=>0,
                "content_markdown"=>null,
                "create_time"=>time(),
                "update_time"=>time(),
            ];
            $res=Db::table('news')->insert($data);
            if($res){
                Db::table("duanzi")->where("id",$detail['id'])->update(["status"=>3]);
            }
        }
    	// 指令输出
    	$output->writeln('结束!');
    }
}
