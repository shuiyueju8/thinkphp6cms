<?php
declare (strict_types=1);

namespace app\command;

use app\admin\model\collect\CollectResult;
use app\admin\model\CoverPre;
use app\admin\model\JokeDetail;
use app\admin\model\News;
use think\console\Command;
use think\console\Input;
use think\console\Output;
use QL\QueryList;
use think\facade\Db;
use think\facade\Log;

class CoverPreUpdate extends Command
{


    protected function configure()
    {
        // 指令配置
        $this->setName('cover_pre_update')
            ->setDescription('更新插入封面');
    }

    protected function execute(Input $input, Output $output)
    {
        $this->task4();
        // 指令输出
        $output->writeln('更新完成');
    }

    protected function task1()
    {
        $jokeDetails = JokeDetail::where("status", 2)->limit(3)->select();
        foreach ($jokeDetails as $detail) {
            $cover = CoverPre::where("type", 7)->order("total,id")->find();
            $data = [
                "news_category_id" => 7,
                "cover" => $cover->cover,
                "title" => $detail->title,
                "abstract" => $detail->description,
                "content" => $detail->content,
                "editor_type" => 0,
                "browse" => 0,
                "content_markdown" => null,
            ];
            $news = new News();
            $res = $news->save($data);
            if ($res) {
                $detail->status = 3;
                $detail->save();
                $cover->total = $cover->total + 1;
                $cover->save();
            }
        }
    }

    protected function task2()
    {
        $results = CollectResult::whereIn("collect_task_id", [10, 11])->where("status", SUCCESS)->select();
        foreach ($results as $result) {
            $content = json_decode($result->result, true);
            foreach ($content as $t) {
                $data = [
                    "type" => 7,
                    "cover" => $t['src'],
                    "gif" => $t['gif'],
                    "title" => $t['title'],
                    "total" => 0
                ];
                $coverPre = new CoverPre();
                $coverPre->save($data);
            }
        }
    }

    protected function task3()
    {
        $results = CollectResult::where("collect_task_id", 15)->where("status", SUCCESS)->select();
        foreach ($results as $t) {
            $this->deal_task15($t);
        }
    }
    protected function task4(){
        $results = CollectResult::where("collect_task_id", 13)->where("status", SUCCESS)->select();
        foreach ($results as $t) {
            $content = json_decode($t->result, true);
            $ql = QueryList::html($content['html']);
            $ql->find(".tg-site")->remove();
            $content1=$ql->getHtml();
            $data = [
                "title" => $content['title'],
                "content" =>$content1
            ];
            Db::table('duanzi')->insert($data);
        }

    }

    protected function deal_task15($result)
    {
        $result = json_decode($result['result'], true);
        $ql = QueryList::html($result['p']);
        $res = $ql->find("p img")->attrs('src')->all();
        $res2 = $ql->find("p")->texts();
        $res3 = [];
        foreach ($res2 as $t) {
            if ($t) {
                if (strpos($t, "很牛帮") === false) {
                    $res3[] = $t;
                }
            }
        }
        $default_tilte = "";
        foreach ($res as $k => $v) {
            $title = isset($res3[$k]) ? $res3[$k] : $default_tilte;
            $title = substr($title, 0, 49);
            $title = $this->removeEmoji($title);
            $default_tilte = $title;
            $data = [
                "type" => 8,
                "cover" => $v,
                "gif" => $v,
                "title" => $title,
                "total" => 0
            ];
            $coverPre = new CoverPre();
            try {
                $coverPre->save($data);
            } catch (\Exception $e) {
                echo $e->getMessage();
                Log::INFO($v . " " . $title);
            }

        }
    }
    public function removeEmoji($text)
    {
        $clean_text = "";
        // Match Emoticons
        $regexEmoticons = '/[\x{1F600}-\x{1F64F}]/u';
        $clean_text = preg_replace($regexEmoticons, '', $text);
        // Match Miscellaneous Symbols and Pictographs
        $regexSymbols = '/[\x{1F300}-\x{1F5FF}]/u';
        $clean_text = preg_replace($regexSymbols, '', $clean_text);
        // Match Transport And Map Symbols
        $regexTransport = '/[\x{1F680}-\x{1F6FF}]/u';
        $clean_text = preg_replace($regexTransport, '', $clean_text);
        // Match Miscellaneous Symbols
        $regexMisc = '/[\x{2600}-\x{26FF}]/u';
        $clean_text = preg_replace($regexMisc, '', $clean_text);
        // Match Dingbats
        $regexDingbats = '/[\x{2700}-\x{27BF}]/u';
        $clean_text = preg_replace($regexDingbats, '', $clean_text);
        return $clean_text;
    }

}
