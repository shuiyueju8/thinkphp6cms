<?php
declare (strict_types = 1);

namespace app\command;

use app\admin\model\CoverPre;
use app\admin\model\JokeDetail;
use think\console\Command;
use think\console\Input;
use think\console\Output;
use think\facade\Db;

class JokeUpdate extends Command
{
    protected function configure()
    {
        // 指令配置
        $this->setName('joke_update')
            ->setDescription('更新joke');
    }

    protected function execute(Input $input, Output $output)
    {

        $jokeDetails=JokeDetail::where("status",2)->limit(5)->select();
        foreach ($jokeDetails as $detail){
            $cover=CoverPre::where("type",7)->order("total,id")->find();

            $cover_cover=$cover->cover;
            $content="<p><img src=\"{$cover->gif}\" alt=\"{$cover->title}\"/></p>\n".$detail->content;
            $cover->total=$cover->total+1;
            $cover->save();
            /********************************尾部增加一张图********************************/
            $cover=CoverPre::where("type",7)->order("total,id")->find();
            $content=$content."<p><img src=\"{$cover->gif}\" alt=\"{$cover->title}\"/></p>";
            $cover->total=$cover->total+1;
            $cover->save();

            $data=[
                "news_category_id"=>7,
                "cover"=>$cover_cover,
                "title"=>$detail->title,
                "abstract"=>$detail->description,
                "content"=>$content,
                "editor_type"=>0,
                "browse"=>0,
                "content_markdown"=>null,
                "create_time"=>time(),
                "update_time"=>time(),
            ];
            $res=Db::table('news')->insert($data);
            if($res){
                $detail->status=3;
                $detail->save();
            }
        }
    	// 指令输出
    	$output->writeln('hello');
    }
}
