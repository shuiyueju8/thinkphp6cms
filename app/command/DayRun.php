<?php
declare (strict_types=1);

namespace app\command;

use app\admin\model\Configs;
use app\admin\model\News;
use think\console\Command;
use think\console\Input;
use think\console\Output;

class DayRun extends Command
{
    protected function configure()
    {
        // 指令配置
        $this->setName('day_run')
            ->setDescription('每日运行');
    }

    protected function execute(Input $input, Output $output)
    {
        $this->count_article_total();
    }

    /*
     * 每日统计网站所有文章数量
     */
    protected function count_article_total()
    {
        $total=News::count();
        $configs=Configs::where("key", 'article_total')->find();
        $configs->value=$total;
        $configs->save();
        \think\facade\Cache::delete('myConfig');
    }
}
