<?php
declare (strict_types = 1);

namespace app\middleware\admin;
class Auth
{
    /**
     * 处理请求
     *
     * @param \think\Request $request
     * @param \Closure $next
     * @return Response
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function handle($request, \Closure $next)
    {
        //验证身份已经登录
        $uid=session('uid');
        if(!$uid){
            return redirect('/admin/login/index');
        }
        if($uid==1){//管理员默认不用设置权限
            return $next($request);
        }
        //验证权限
        $auth = new \Auth();
        if (!$auth->check($request->controller() . '/' . $request->action(), session('uid'))) {
            if ($request->isAjax()) {
                $data = ['code' => 403, 'msg' => '当前没有权限，请联系管理员获取权限！', 'data' => null];
                echo json_encode($data);
                exit;
            } else {
                echo '403';exit;
//                $this->redirect('/403.html');
            }
        }

        return $next($request);
    }
}
