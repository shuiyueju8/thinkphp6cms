<?php
declare (strict_types=1);

namespace app\controller;
use app\admin\model\Banner;
use app\admin\model\News;
use app\admin\model\NewsCategory;
use app\admin\model\Tags;
use app\common\Sidebar;
use think\facade\Db;

class Category extends IndexBase
{
    public function index()
    {
        $id=request()->param('id');
        $id=$id?$id:1;
        $new_category=NewsCategory::find($id);
        $categorys=NewsCategory::select();
        $child_category=unlimitedSort($categorys,$id);
        $ids=[$id];
        $ids=array_merge($ids,array_column($child_category,'id'));
        $news=News::whereIn('news_category_id',$ids)->order('id','desc')->paginate(10);
        $baners_ad2 = Banner::where('type', '右侧中部')->select();
        $sidebar=new Sidebar();
        $sidebar=$sidebar->add_sidebar([
            ['sidebar'=>'site_recommend','category_id'=>$id,'total'=>4],
            ['sidebar'=>'recent_browse','category_id'=>$id,'total'=>5],
            ['sidebar'=>'ad','url'=>$baners_ad2[0]['url'],'cover'=>$baners_ad2[0]['cover']],
            ['sidebar'=>'hot_tags'],
            ['sidebar'=>'ad','url'=>$baners_ad2[1]['url'],'cover'=>$baners_ad2[1]['cover']],
            ['sidebar'=>'think_you_like','category_id'=>$id,'total'=>6],
        ]);
        return view('index',compact('new_category','news','sidebar'));
    }
}
