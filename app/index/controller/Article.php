<?php
declare (strict_types=1);

namespace app\index\controller;
use app\admin\model\Banner;
use app\admin\model\News;
use app\admin\model\NewsTags;
use app\common\Sidebar;
use think\facade\Db;
use think\captcha\facade\Captcha;

class Article extends IndexBase
{
    public function index()
    {
        $id=request()->param('id');
        $id=$id?$id:1;
        //阅读文章自增长
        News::where('id',$id)->inc('browse')->update();
        $article=News::with(array('newsCategory','tags','comments'))->withCount('comments')->find($id);
        $site_title=$article->title.'_思议博客';
        $site_description=$article->abstract;

        $baners_ad2 = Banner::where('type', '右侧中部')->select();
        //查询上一篇
        $up_article=News::where('id','<',$id)->order('id desc')->limit(1)->find();
        //查询下一篇
        $down_article=News::where('id','>',$id)->order('id')->limit(1)->find();
        //随机图文四篇
        $rand_news = News::orderRaw('rand()')->limit(4)->select();
        //相关文章6篇
        $relevant_news=array();
        $tags=$article->tags->toArray();
        if ($tags){
            $news_tags=NewsTags::where('tags_id','in',array_column($article->tags->toArray(),'id'))->limit(7)->select()->toArray();
            if($news_tags){
                $news_ids=array_diff(array_column($news_tags,'news_id'),array($id));
                $relevant_news=News::where('id','in',$news_ids)->limit(7)->select();
            }
        }
        $sidebar=new Sidebar();
        $sidebar=$sidebar->add_sidebar([
            ['sidebar'=>'recent_browse','category_id'=>$id,'total'=>5],
            ['sidebar'=>'site_recommend','category_id'=>$id,'total'=>3],
            ['sidebar'=>'ad','url'=>$baners_ad2[0]['url'],'cover'=>$baners_ad2[0]['cover']],
            ['sidebar'=>'hot_tags'],
            ['sidebar'=>'ad','url'=>$baners_ad2[1]['url'],'cover'=>$baners_ad2[1]['cover']],
            ['sidebar'=>'tongji_base'],
        ]);
        $data=compact(
            'article',
            'up_article',
            'down_article',
            'site_description',
            'site_title',
            'rand_news',
            'relevant_news',
            'sidebar'
        );
        if($article['editor_type']){
            return view('index_use_markdown',$data);
        }else{
            return view('index',$data);
        }
    }
    //赞该文章
    public function search()
    {
        $keyboard=request()->param('keyboard');
        $news=News::where('title','like',"%{$keyboard}%")->with('newsCategory')->paginate(['list_rows'=> 10,'query'=>['keyboard'=>$keyboard]]);
        $baners_ad2 = Banner::where('type', '右侧中部')->select();
        $sidebar=new Sidebar();
        $sidebar=$sidebar->add_sidebar([
            ['sidebar'=>'site_recommend','total'=>4],
            ['sidebar'=>'recent_browse','total'=>5],
            ['sidebar'=>'ad','url'=>$baners_ad2[0]['url'],'cover'=>$baners_ad2[0]['cover']],
            ['sidebar'=>'hot_tags'],
            ['sidebar'=>'ad','url'=>$baners_ad2[1]['url'],'cover'=>$baners_ad2[1]['cover']],
            ['sidebar'=>'think_you_like','total'=>6],
        ]);
        return view('search',
            compact(
                'news',
                'keyboard',
                'sidebar')
        );
    }

    //赞该文章
    public function zan()
    {
        $id=request()->param('id');
        $id=$id?$id:1;
        News::where('id',$id)->inc('up')->update();
        return xjson('点赞成功！');
    }

    public function verify()
    {
        return Captcha::create();
    }

}
