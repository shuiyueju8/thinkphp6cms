<?php /*a:2:{s:50:"D:\WWW6\tp6-xadmin\app\admin\view\index\index.html";i:1603026635;s:50:"D:\WWW6\tp6-xadmin\app\admin\view\public\base.html";i:1603011831;}*/ ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title><?php echo htmlentities($site_title); ?>后台管理系统</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8"/>
    <link rel="stylesheet" href="/xadmin/css/font.css">
    <link rel="stylesheet" href="/xadmin/css/font-awesome.css">
    <link rel="stylesheet" href="/xadmin/lib/layui/css/layui.css">
    <link rel="stylesheet" href="/xadmin/css/xadmin.css">
    <link rel="stylesheet" href="/xadmin/css/xiugai.css">
    
    <script type="text/javascript" src="/xadmin/lib/layui/layui.js" charset="utf-8"></script>
    <script type="text/javascript" src="/xadmin/js/xadmin.js"></script>
    <script type="text/javascript" src="https://cdn.bootcss.com/jquery/3.2.1/jquery.min.js"></script>
    <!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
    <!--[if lt IE 9]>
    <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
    <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

    <body class="index">
    <!-- 顶部开始 -->
    <div class="container">
        <div class="logo">
            <a href="./index.html"><?php echo htmlentities($site_title); ?>后台管理系统</a></div>
        <div class="left_open">
            <a><i title="展开左侧栏" class="iconfont">&#xe699;</i></a>
        </div>
        <ul class="layui-nav left fast-add" lay-filter="">
            <li class="layui-nav-item">
                <a href="javascript:;" class="clear_confs">清除配置缓存</a>
            </li>
            <li class="layui-nav-item">
                <a href="javascript:;">+新增</a>
                <dl class="layui-nav-child">
                    <!-- 二级菜单 -->
                    <dd>
                        <a onclick="xadmin.open('最大化','http://www.baidu.com','','',true)">
                            <i class="iconfont">&#xe6a2;</i>弹出最大化</a></dd>
                    <dd>
                        <a onclick="xadmin.open('弹出自动宽高','http://www.baidu.com')">
                            <i class="iconfont">&#xe6a8;</i>弹出自动宽高</a></dd>
                    <dd>
                        <a onclick="xadmin.open('弹出指定宽高','http://www.baidu.com',500,300)">
                            <i class="iconfont">&#xe6a8;</i>弹出指定宽高</a></dd>
                    <dd>
                        <a onclick="xadmin.add_tab('在tab打开','member-list.html')">
                            <i class="iconfont">&#xe6b8;</i>在tab打开</a></dd>
                    <dd>
                        <a onclick="xadmin.add_tab('在tab打开刷新','member-del.html',true)">
                            <i class="iconfont">&#xe6b8;</i>在tab打开刷新</a>
                    </dd>
                </dl>
            </li>
        </ul>
        <ul class="layui-nav right" lay-filter="">
            <li class="layui-nav-item">
                <a href="javascript:;">admin</a>
                <dl class="layui-nav-child">
                    <!-- 二级菜单 -->
                    <dd>
                        <a onclick="xadmin.open('个人信息','http://www.baidu.com')">个人信息</a></dd>
                    <dd>
                        <a onclick="xadmin.open('切换帐号','http://www.baidu.com')">切换帐号</a></dd>
                    <dd>
                        <a href="<?php echo url('Login/loginOut'); ?>">退出</a></dd>
                </dl>
            </li>
            <li class="layui-nav-item to-index">
                <a href="/">前台首页</a></li>
        </ul>
    </div>
    <!-- 顶部结束 -->
    <!-- 中部开始 -->
    <!-- 左侧菜单开始 -->
    <div class="left-nav">
        <div id="side-nav">
            <ul id="nav">
                <?php if(is_array($menus) || $menus instanceof \think\Collection || $menus instanceof \think\Paginator): $i = 0; $__LIST__ = $menus;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$menu): $mod = ($i % 2 );++$i;?>
                    <li>
                        <?php if($menu['children']): ?>
                            <a href="javascript:;">
                                <i class="left-nav-li <?php echo htmlentities($menu['icon']); ?>" lay-tips="<?php echo htmlentities($menu['name']); ?>"></i>
                                <cite><?php echo htmlentities($menu['name']); ?></cite>
                                <i class="iconfont nav_right">&#xe697;</i>
                            </a>
                            <ul class="sub-menu">
                                <?php if(is_array($menu['children']) || $menu['children'] instanceof \think\Collection || $menu['children'] instanceof \think\Paginator): $i = 0; $__LIST__ = $menu['children'];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$me): $mod = ($i % 2 );++$i;?>
                                    <li>
                                        <?php if($me['children']): ?>
                                            <a href="javascript:;">
                                                <i class="left-nav-li <?php echo htmlentities($me['icon']); ?>" lay-tips="<?php echo htmlentities($me['name']); ?>"></i>
                                                <cite><?php echo htmlentities($me['name']); ?></cite>
                                                <i class="iconfont nav_right">&#xe697;</i>
                                            </a>
                                            <ul class="sub-menu">
                                                <?php if(is_array($me['children']) || $me['children'] instanceof \think\Collection || $me['children'] instanceof \think\Paginator): $i = 0; $__LIST__ = $me['children'];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$m): $mod = ($i % 2 );++$i;?>
                                                    <li>
                                                        <a onclick="xadmin.add_tab('<?php echo htmlentities($m['name']); ?>','<?php echo htmlentities($m['url']); ?>',true)">>
                                                            <i class="<?php echo htmlentities($m['icon']); ?>"></i>
                                                            <cite><?php echo htmlentities($m['name']); ?></cite>
                                                        </a>
                                                    </li>
                                                <?php endforeach; endif; else: echo "" ;endif; ?>
                                            </ul>
                                            <?php else: ?>
                                            <a onclick="xadmin.add_tab('<?php echo htmlentities($me['name']); ?>','<?php echo htmlentities($me['url']); ?>',true)">
                                                <i class="<?php echo htmlentities((isset($me['icon']) && ($me['icon'] !== '')?$me['icon']:'fa fa-home')); ?>"></i>
                                                <cite><?php echo htmlentities($me['name']); ?></cite>
                                            </a>
                                        <?php endif; ?>
                                    </li>
                                <?php endforeach; endif; else: echo "" ;endif; ?>
                            </ul>
                            <?php else: ?>
                            <a onclick="xadmin.add_tab('<?php echo htmlentities($menu['name']); ?>','<?php echo htmlentities($menu['url']); ?>',true)">
                                <i class="layui-icon left-nav-li <?php echo htmlentities($menu['icon']); ?>" lay-tips="<?php echo htmlentities($menu['name']); ?>"></i>
                                <cite><?php echo htmlentities($menu['name']); ?></cite>
                            </a>
                        <?php endif; ?>
                    </li>
                <?php endforeach; endif; else: echo "" ;endif; ?>
            </ul>
        </div>
    </div>
    <!-- <div class="x-slide_left"></div> -->
    <!-- 左侧菜单结束 -->
    <!-- 右侧主体开始 -->
    <div class="page-content">
        <div class="layui-tab tab" lay-filter="xbs_tab" lay-allowclose="false">
            <ul class="layui-tab-title">
                <li class="home">
                    <i class="layui-icon">&#xe68e;</i>我的桌面</li></ul>
            <div class="layui-unselect layui-form-select layui-form-selected" id="tab_right">
                <dl>
                    <dd data-type="this">关闭当前</dd>
                    <dd data-type="other">关闭其它</dd>
                    <dd data-type="all">关闭全部</dd></dl>
            </div>
            <div class="layui-tab-content">
                <div class="layui-tab-item layui-show">
                    <iframe src='<?php echo url("welcome"); ?>' frameborder="0" scrolling="yes" class="x-iframe"></iframe>
                </div>
            </div>
            <div id="tab_show"></div>
        </div>
    </div>
    <div class="page-content-bg"></div>
    <style id="theme_style"></style>
    <!-- 右侧主体结束 -->
    <!-- 中部结束 -->
    </body>
    <script>
        $('.clear_confs').click(function () {
            $.get('<?php echo url("configs/clear"); ?>',function (res) {
                if (res.code == 0) {
                    layer.alert("清除缓存成功!", {icon: 6});
                } else {
                    layer.msg(res.msg, {icon: 5});
                }
            });
        });
    </script>


</html>