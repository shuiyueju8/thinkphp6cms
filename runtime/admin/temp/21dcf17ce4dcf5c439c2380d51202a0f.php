<?php /*a:2:{s:49:"D:\WWW6\tp6-xadmin\app\admin\view\news\index.html";i:1603017010;s:50:"D:\WWW6\tp6-xadmin\app\admin\view\public\form.html";i:1603015784;}*/ ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title><?php echo htmlentities($html_title); ?>-<?php echo htmlentities($site_title); ?>后台管理系统</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8"/>
    <link rel="stylesheet" href="/xadmin/css/font.css">
    <link rel="stylesheet" href="/xadmin/css/font-awesome.css">
    <link rel="stylesheet" href="/static/layui/css/layui.css">
    <link rel="stylesheet" href="/xadmin/css/xadmin.css">
    <link rel="stylesheet" href="/xadmin/css/xiugai.css">
    
    <script type="text/javascript" src="/static/layui/layui.js" charset="utf-8"></script>
    <script type="text/javascript" src="/xadmin/js/xadmin.js"></script>
    <script type="text/javascript" src="https://cdn.bootcss.com/jquery/3.2.1/jquery.min.js"></script>
    <!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
    <!--[if lt IE 9]>
    <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
    <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div class="layui-fluid">
    <div class="layui-row layui-col-space15">
        <div class="layui-col-md12">
            <div class="layui-card">
                
    <div class="layui-card-header ">
        <span class="layui-breadcrumb">
          <a href="/admin" target="_blank">首页</a>
          <a href="javascript:;">资讯管理</a>
          <a><cite>资讯列表</cite></a>
        </span>
        <a class="layui-btn layui-btn-sm" style="line-height:1.6em;margin-top:3px;float:right"
           href="javascript:location.replace(location.href);" title="刷新">
            <i class="layui-icon layui-icon-refresh-3" style="line-height:30px"></i></a>
    </div>
    <div class="layui-card-body " style="line-height: 38px">
        <form class="layui-form" id="search_form">
            <div class="layui-form-item">
                <div class="layui-input-inline">
                    <select name="news_category_id" id="pid-select" lay-filter="pid-select">
                        <option value='' data-level="1">请选择分类</option>
                        <?php if(is_array($treeList) || $treeList instanceof \think\Collection || $treeList instanceof \think\Paginator): $i = 0; $__LIST__ = $treeList;if( count($__LIST__)==0 ) : echo "暂时没有数据" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?>
                            <option value="<?php echo htmlentities($vo['id']); ?>" data-level="<?php echo htmlentities($vo['level']); ?>"><?php echo htmlentities($vo['_title']); ?></option>
                        <?php endforeach; endif; else: echo "暂时没有数据" ;endif; ?>
                    </select>
                </div>
                <div class="layui-input-inline" style="width: 400px">
                    <input type="text" name="title" placeholder="请输入资讯标题" class="layui-input" value="">
                </div>
                <div class="layui-input-inline"
                     style="display: inline-block;float: left;width: 185px;line-height: 38px">
                    <input class="layui-input" type="checkbox" name="top" lay-skin="switch" lay-text="置顶|置顶">
                    <input class="layui-input" type="checkbox" name="hot" lay-skin="switch" lay-text="热门|热门">
                    <input class="layui-input" type="checkbox" name="recommend" lay-skin="switch" lay-text="推荐|推荐">
                </div>

                <div class="layui-input-inline" style="width: 400px">
                    <button class="layui-btn-sm layui-btn" lay-submit="" lay-filter="search">搜索</button>
                    <button type="reset" class="layui-btn-sm layui-btn layui-btn-primary reset">重置</button>
                    <button type="button" class="layui-btn layui-btn-danger layui-btn-sm delAll">批量删除</button>
                    <button type="button" class="layui-btn layui-btn-sm"
                            onclick="parent.xadmin.add_tab('添加资讯','<?php echo url('add'); ?>')">添加
                    </button>
                </div>
            </div>
        </form>

    </div>
    <div class="layui-card-body ">
        <table class="layui-hide" id="test" lay-filter="demo"></table>
        <script type="text/html" id="barDemo">
            <a class="layui-btn layui-btn-normal layui-btn-xs" lay-event="edit"><i
                    class="layui-icon layui-icon-edit"></i>编辑</a>
            <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del"><i
                    class="layui-icon layui-icon-delete"></i>删除</a>
        </script>
    </div>

            </div>
        </div>
    </div>
</div>
</body>


    <script>
        layui.use(['table', 'jquery','element','form'], function () {
            var table = layui.table
            $ = layui.jquery,
                element = layui.element,
                form = layui.form,
                where = {};
            element.render();
            render_table();
            //监听搜索提交
            form.on('submit(search)', function (data) {
                where = data.field;
                render_table(where);
                return false;
            });
            //重置
            $('.reset').on('click', function () {
                render_table();
                $('#search_form')[0].reset();
                layui.form.render();
                where = {};
                return false;
            });

            function render_table(where) {
                var render_obj = {
                    elem: '#test'
                    , url: '/admin/news/tree'
                    , cellMinWidth: 70
                    , cols: [[
                        {type: 'checkbox'}
                        , {field: 'id', title: 'ID', width: 80, align: 'center', sort: true}
                        , {
                            field: 'cover', title: '封面图', align: 'center', height: 150, width: 200
                            , templet: function (d) {
                                return "<img src=\"" + d.cover + "\"></img>";
                            }
                        }
                        , {
                            field: 'title', title: '资讯标题', templet: function (d) {
                                    return `<a href="/article/${d.id}.html" target="_blank">${d.title}</a>`;
                            }
                        }
                        , {
                            field: 'newsCategory', width: 150, title: '所属分类', templet: function (d) {
                                return d.newsCategory.title ? d.newsCategory.title : "未选择分类";
                            }
                        }
                        , {
                            title: '置顶|热门|推荐', align: 'center', width: 300
                            , templet: function (d) {
                                let top = d.top == 1 ? 'checked' : '';
                                let res = '<input type="checkbox" name="top" lay-filter="filter" value="' + d.id + '" lay-skin="switch" lay-text="置顶|置顶"' + top + '>';

                                let hot = d.hot == 1 ? 'checked' : '';
                                res = res + ' <input type="checkbox" name="hot" lay-filter="filter" value="' + d.id + '" lay-skin="switch" lay-text="热门|热门"' + hot + '>';

                                let recommend = d.recommend == 1 ? 'checked' : '';
                                res = res + ' <input type="checkbox" name="recommend" lay-filter="filter" value="' + d.id + '" lay-skin="switch" lay-text="推荐|推荐"' + recommend + '>';
                                return res;
                            }
                        }
                        , {title: '操作', width: 200, align: 'center', toolbar: '#barDemo'}
                    ]]
                    , page: true
                };
                if (where) {
                    render_obj.where = where;//追加搜索条件
                }
                table.render(render_obj);
            }

            //监听工具条
            table.on('tool(demo)', function (obj) {
                var data = obj.data;
                switch (obj.event) {
                    case 'detail':
                        layer.msg('ID：' + data.id + ' 的查看操作');
                        break;
                    case 'del':
                        layer.confirm('确定要删除该斑斓图吗？', function (index) {
                            $.ajax({
                                url: '/admin/news/delete',
                                method: 'post',
                                data: {id: data.id},
                                dataType: 'JSON',
                                success: function (res) {
                                    if (res.code == 0) {
                                        obj.del();
                                        layer.msg('删除成功', {icon: 1});
                                    } else {
                                        layer.msg(res.msg, {icon: 5});
                                    }
                                },
                                error: function (data) {
                                    layer.msg('服务器繁忙', {icon: 5});
                                }
                            });
                            layer.close(index);
                        });
                        break;
                    case 'edit':
                        xadmin.open('编辑', '/admin/news/edit?id=' + data.id);
                        // layer.alert('编辑行：<br>'+ JSON.stringify(data))
                        break;
                    default:
                        break;
                }
            });
            //监听置顶|热门|推荐
            form.on('switch(filter)', function (obj) {
                let data = {};
                data[this.name] = obj.elem.checked ? 1 : 0;
                let id = this.value;
                let __this = this;
                $.ajax({
                    url: '/admin/news/ajax/id/' + id,
                    method: 'post',
                    data: data,
                    dataType: 'JSON',
                    success: function (res) {
                        if (res.code == 0) {
                            layer.msg('修改成功', {icon: 1});
                        } else {
                            __this.checked = true;
                            form.render();
                            layer.msg(res.msg, {icon: 5});
                        }
                    },
                    error: function (data) {
                        layer.msg('服务器繁忙', {icon: 5});
                    }
                });
            });
            $('.delAll').on('click', function () {
                var checkStatus = table.checkStatus('test')
                    , data = checkStatus.data
                    , ids = '';
                if (data.length > 0) {
                    for (var i = 0; i < data.length; i++) {
                        ids = ids + data[i].id + ',';
                    }
                    var ids = (ids.substring(ids.length - 1) == ',') ? ids.substring(0, ids.length - 1) : ids;
                    layer.confirm('确定要删除' + data.length + '个斑斓图吗？', function (index) {
                        $.ajax({
                            url: '/admin/news/delete',
                            method: 'post',
                            data: {id: ids},
                            dataType: 'JSON',
                            success: function (res) {
                                if (res.code == 0) {
                                    table.reload('test');
                                    layer.msg('删除成功', {icon: 1});
                                } else {
                                    layer.msg(res.msg, {icon: 5});
                                }
                            },
                            error: function (data) {
                                layer.msg('服务器繁忙', {icon: 5});
                            }
                        });
                        layer.close(index);
                    });
                }
            });

        });
    </script>

</html>