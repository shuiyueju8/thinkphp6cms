<?php /*a:2:{s:58:"D:\WWW6\tp6-xadmin\app\admin\view\news_category\index.html";i:1604037874;s:50:"D:\WWW6\tp6-xadmin\app\admin\view\public\form.html";i:1603015784;}*/ ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title><?php echo htmlentities($html_title); ?>-<?php echo htmlentities($site_title); ?>后台管理系统</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8"/>
    <link rel="stylesheet" href="/xadmin/css/font.css">
    <link rel="stylesheet" href="/xadmin/css/font-awesome.css">
    <link rel="stylesheet" href="/static/layui/css/layui.css">
    <link rel="stylesheet" href="/xadmin/css/xadmin.css">
    <link rel="stylesheet" href="/xadmin/css/xiugai.css">
    
    <script type="text/javascript" src="/static/layui/layui.js" charset="utf-8"></script>
    <script type="text/javascript" src="/xadmin/js/xadmin.js"></script>
    <script type="text/javascript" src="https://cdn.bootcss.com/jquery/3.2.1/jquery.min.js"></script>
    <!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
    <!--[if lt IE 9]>
    <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
    <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div class="layui-fluid">
    <div class="layui-row layui-col-space15">
        <div class="layui-col-md12">
            <div class="layui-card">
                
    <div class="layui-card-header ">
        <span class="layui-breadcrumb">
          <a href="/admin" target="_blank">首页</a>
          <a href="javascript:;">资讯管理</a>
          <a><cite>资讯分类列表</cite></a>
        </span>
        <a class="layui-btn layui-btn-sm" style="line-height:1.6em;margin-top:3px;float:right"
           href="javascript:location.replace(location.href);" title="刷新">
            <i class="layui-icon layui-icon-refresh-3" style="line-height:30px"></i></a>
    </div>
    <div class="layui-card-body ">
        <button class="layui-btn layui-btn-danger layui-btn-sm" onclick="delAll()"><i class="layui-icon"></i>批量删除
        </button>
        <button class="layui-btn layui-btn-sm" onclick="parent.{<?php echo add_tab('新增资讯分类','add'); ?>}" ><i class="layui-icon"></i>添加
        </button>
        <button class="layui-btn layui-btn-warm layui-btn-sm" onclick="openAll();"><i class="layui-icon"></i>展开或折叠全部
        </button>
        <button class="layui-btn layui-btn-normal layui-btn-sm" onclick="reload()"><i
                class="layui-icon layui-icon-refresh"></i>刷新
        </button>
    </div>
    <div class="layui-card-body ">
        <table class="layui-table" id="treeTable" lay-filter="treeTable"></table>
    </div>

            </div>
        </div>
    </div>
</div>
</body>


    <script>
        var editObj = null, ptable = null, treeGrid = null, tableId = 'treeTable', layer = null, $ = null;
        layui.config({
            base: '/xadmin/lib/layui/lay/modules/'
        }).extend({
            treeGrid: 'treeGrid',
        }).use(['jquery', 'treeGrid', 'layer','element'], function () {
            var $ = layui.jquery;
            var table = layui.table;
            layui.element.render();
            treeGrid = layui.treeGrid;//很重要
            layer = layui.layer;
            ptable = treeGrid.render({
                id: tableId
                , elem: '#' + tableId
                , url: '/admin/news_category/tree2'
                , cellMinWidth: 100
                , idField: 'id'//必須字段
                , treeId: 'id'//树形id字段名称
                , treeUpId: 'pid'//树形父id字段名称
                , treeShowName: 'title'//以树形式显示的字段
                , heightRemove: [".dHead", 10]//不计算的高度,表格设定的是固定高度，此项不生效
                , height: '100%'
                , isFilter: false
                , iconOpen: false//是否显示图标【默认显示】
                , isOpenDefault: false//分类默认是展开还是折叠【默认展开】
                , loading: true
                , method: 'post'
                , isPage: false
                , cols: [[
                    // ,{type:'radio'}
                    {type: 'checkbox', sort: true}
                    , {type: 'numbers'}
                    , {field: 'title', width: 300, title: '资讯分类名称'}
                    , {field: 'sorts', width: 100, title: '排序'}
                    , {
                        width: 100, title: '是否显示', align: 'center'/*toolbar: '#barDemo'*/
                        , templet: function (d) {
                            if (d.status == 1) {
                                return '<a lay-event="status">' + '<input type="checkbox" lay-event="status-close" name="status" lay-skin="switch" lay-text="显示|隐藏" checked>' + '</a>';
                            } else {
                                return '<a lay-event="status">' + '<input type="checkbox" lay-event="status-open" name="status" lay-skin="switch" lay-text="显示|隐藏" >' + '</a>';
                            }
                        }
                    }
                    , {
                        width: 250, title: '操作', align: 'center'/*toolbar: '#barDemo'*/
                        , templet: function (d) {
                            var disable = '';
                            if (d.level > 2) {
                                var addBtn = '<a class="layui-btn layui-btn-xs layui-btn-warm" lay-event="copy"><i class="layui-icon layui-icon-add-1"></i>复制编辑分类</a>';
                            } else {
                                var addBtn = '<a class="layui-btn layui-btn-xs" lay-event="add"><i class="layui-icon layui-icon-add-1"></i>添加子分类</a>';
                            }
                            var editBtn = '<a class="layui-btn layui-btn-normal layui-btn-xs" lay-event="edit"><i class="layui-icon layui-icon-edit"></i>编辑</a>';
                            var delBtn = '<a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del"><i class="layui-icon layui-icon-delete">删除</a>';
                            return addBtn + editBtn + delBtn;
                        }
                    }
                ]]
                , parseData: function (res) 
            });
            //监听单元格编辑
            treeGrid.on('tool(' + tableId + ')', function (obj) {
                if (obj.event === 'del')  else if (obj.event === "add") else if (obj.event === "copy")  else if (obj.event === "edit")  else if (obj.event === "status") );
                } else if (obj.event === "menu") );
                }
            });
            function ajax_update(id,json) {
                $.ajax({
                    url: '/admin/news_category/ajax/id/' + id,
                    method: 'post',
                    data: json,
                    dataType: 'JSON',
                    success: function (res) {
                        if (res.code == 0) {
                            layer.msg('修改成功', {icon: 1});
                        } else {
                            layer.msg(res.msg, {icon: 5});
                            reload();
                        }
                    },
                    error: function (data) {
                        layer.msg('服务器繁忙', {icon: 5});
                        reload();
                    }
                })
            }
        });



        function del(obj) {
            layer.confirm("你确定删除数据吗？此操作不能撤销，请谨慎操作！", {icon: 3, title: '提示'},
                function (index) ;
                    data.id = obj.data.id;
                    $.ajax({
                        url: '/admin/news_category/delete',
                        method: 'post',
                        data: data,
                        dataType: 'JSON',
                        success: function (res) {
                            //基本演示
                            if (res.code == 0) {
                                layer.msg(res.msg, {icon: 1});
                                obj.del();
                            } else {
                                layer.msg(res.msg, {icon: 5});
                            }
                        },
                        error: function (data) {
                            layer.msg('服务器繁忙', {icon: 5});
                        }
                    });
                    layer.close(index);
                }, function (index) 
            );
        }
        //删除所选的全部
        function delAll() {
            var checkStatus = treeGrid.checkStatus(tableId)
                , data = checkStatus.data;
            var ids = [];
            for (var i = 0; i < data.length; i++) {
                ids.push(data[i].id);
            }
            layer.confirm("你确定删除" + data.length + "条数据吗？此操作不能撤销！", {icon: 3, title: '提示'},
                function (index) ;
                    sdata.id = ids;
                    $.ajax({
                        url: '/admin/news_category/delete',
                        method: 'post',
                        data: sdata,
                        dataType: 'JSON',
                        success: function (res) {
                            //基本演示
                            if (res.code == 0) {
                                layer.msg(res.msg, {icon: 1});
                                checkStatus.del();
                            } else {
                                layer.msg(res.msg, {icon: 5});
                            }
                        },
                        error: function (data) {
                            layer.msg('服务器繁忙', {icon: 5});
                        }
                    });
                    layer.close(index);
                }, function (index) 
            );

        }
        function openAll() {
            var treedata=treeGrid.getDataTreeList(tableId);
            treeGrid.treeOpenAll(tableId,!treedata[0][treeGrid.config.cols.isOpen]);
        }
        function reload() {
            treeGrid.reload(tableId, {
                page: {
                    curr: 1
                }
            });
        }
    </script>

</html>