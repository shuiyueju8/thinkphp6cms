<?php /*a:2:{s:52:"D:\WWW6\tp6-xadmin\app\admin\view\index\welcome.html";i:1603025205;s:50:"D:\WWW6\tp6-xadmin\app\admin\view\public\base.html";i:1603011831;}*/ ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title><?php echo htmlentities($site_title); ?>后台首页</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8"/>
    <link rel="stylesheet" href="/xadmin/css/font.css">
    <link rel="stylesheet" href="/xadmin/css/font-awesome.css">
    <link rel="stylesheet" href="/xadmin/lib/layui/css/layui.css">
    <link rel="stylesheet" href="/xadmin/css/xadmin.css">
    <link rel="stylesheet" href="/xadmin/css/xiugai.css">
    
    <script type="text/javascript" src="/xadmin/lib/layui/layui.js" charset="utf-8"></script>
    <script type="text/javascript" src="/xadmin/js/xadmin.js"></script>
    <script type="text/javascript" src="https://cdn.bootcss.com/jquery/3.2.1/jquery.min.js"></script>
    <!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
    <!--[if lt IE 9]>
    <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
    <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

    <body>
    <div class="layui-fluid">
        <div class="layui-row layui-col-space15">
            <div class="layui-col-md12">
                <div class="layui-card">
                    <div class="layui-card-body ">
                        <blockquote class="layui-elem-quote">欢迎管理员：
                            <span class="x-red"><?php echo htmlentities($user['username']); ?></span>！当前时间:<?php echo htmlentities($serverinfo['server_time']); ?>
                    </div>
                </div>
            </div>
            <div class="layui-col-md12">
                <div class="layui-card">
                    <div class="layui-card-header">数据统计</div>
                    <div class="layui-card-body ">
                        <ul class="layui-row layui-col-space10 layui-this x-admin-carousel x-admin-backlog">
                            <li class="layui-col-md2 layui-col-xs6">
                                <a href="javascript:;" class="x-admin-backlog-body">
                                    <h3>文章数</h3>
                                    <p>
                                        <cite>66</cite></p>
                                </a>
                            </li>
                            <li class="layui-col-md2 layui-col-xs6">
                                <a href="javascript:;" class="x-admin-backlog-body">
                                    <h3>会员数</h3>
                                    <p>
                                        <cite>12</cite></p>
                                </a>
                            </li>
                            <li class="layui-col-md2 layui-col-xs6">
                                <a href="javascript:;" class="x-admin-backlog-body">
                                    <h3>回复数</h3>
                                    <p>
                                        <cite>99</cite></p>
                                </a>
                            </li>
                            <li class="layui-col-md2 layui-col-xs6">
                                <a href="javascript:;" class="x-admin-backlog-body">
                                    <h3>商品数</h3>
                                    <p>
                                        <cite>67</cite></p>
                                </a>
                            </li>
                            <li class="layui-col-md2 layui-col-xs6">
                                <a href="javascript:;" class="x-admin-backlog-body">
                                    <h3>文章数</h3>
                                    <p>
                                        <cite>67</cite></p>
                                </a>
                            </li>
                            <li class="layui-col-md2 layui-col-xs6 ">
                                <a href="javascript:;" class="x-admin-backlog-body">
                                    <h3>文章数</h3>
                                    <p>
                                        <cite>6766</cite></p>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="layui-col-sm6 layui-col-md3">
                <div class="layui-card">
                    <div class="layui-card-header">下载
                        <span class="layui-badge layui-bg-cyan layuiadmin-badge">月</span></div>
                    <div class="layui-card-body  ">
                        <p class="layuiadmin-big-font">33,555</p>
                        <p>新下载
                            <span class="layuiadmin-span-color">10%
                                    <i class="layui-inline layui-icon layui-icon-face-smile-b"></i></span>
                        </p>
                    </div>
                </div>
            </div>
            <div class="layui-col-sm6 layui-col-md3">
                <div class="layui-card">
                    <div class="layui-card-header">下载
                        <span class="layui-badge layui-bg-cyan layuiadmin-badge">月</span></div>
                    <div class="layui-card-body ">
                        <p class="layuiadmin-big-font">33,555</p>
                        <p>新下载
                            <span class="layuiadmin-span-color">10%
                                    <i class="layui-inline layui-icon layui-icon-face-smile-b"></i></span>
                        </p>
                    </div>
                </div>
            </div>
            <div class="layui-col-sm6 layui-col-md3">
                <div class="layui-card">
                    <div class="layui-card-header">下载
                        <span class="layui-badge layui-bg-cyan layuiadmin-badge">月</span></div>
                    <div class="layui-card-body ">
                        <p class="layuiadmin-big-font">33,555</p>
                        <p>新下载
                            <span class="layuiadmin-span-color">10%
                                    <i class="layui-inline layui-icon layui-icon-face-smile-b"></i></span>
                        </p>
                    </div>
                </div>
            </div>
            <div class="layui-col-sm6 layui-col-md3">
                <div class="layui-card">
                    <div class="layui-card-header">下载
                        <span class="layui-badge layui-bg-cyan layuiadmin-badge">月</span></div>
                    <div class="layui-card-body ">
                        <p class="layuiadmin-big-font">33,555</p>
                        <p>新下载
                            <span class="layuiadmin-span-color">10%
                                    <i class="layui-inline layui-icon layui-icon-face-smile-b"></i></span>
                        </p>
                    </div>
                </div>
            </div>
            <div class="layui-col-md12">
                <div class="layui-card">
                    <div class="layui-card-header">系统信息</div>
                    <div class="layui-card-body ">
                        <table class="layui-table">
                            <tbody>
                            <tr>
                                <th>系统版本</th>
                                <td>2.0.1</td></tr>
                            <tr>
                                <th>服务器地址</th>
                                <td><?php echo htmlentities($serverinfo['domain_ip']); ?></td></tr>
                            <tr>
                                <th>操作系统</th>
                                <td><?php echo htmlentities($serverinfo['server_os']); ?></td></tr>
                            <tr>
                                <th>运行环境</th>
                                <td><?php echo htmlentities($serverinfo['server_soft']); ?></td></tr>
                            <tr>
                                <th>PHP版本</th>
                                <td><?php echo htmlentities($serverinfo['php_version']); ?></td></tr>
                            <tr>
                                <th>PHP运行方式</th>
                                <td><?php echo htmlentities($serverinfo['PHP_run']); ?></td></tr>
                            <tr>
                                <th>MYSQL版本</th>
                                <td><?php echo htmlentities($serverinfo['mysql_version']); ?></td></tr>
                            <tr>
                                <th>ThinkPHP</th>
                                <td><?php echo $serverinfo['Think_version']; ?></td></tr>
                            <tr>
                                <th>上传附件限制</th>
                                <td><?php echo htmlentities($serverinfo['max_upload_size']); ?></td></tr>
                            <tr>
                                <th>执行时间限制</th>
                                <td><?php echo htmlentities($serverinfo['max_execution_time']); ?></td></tr>
                            <tr>
                                <th>剩余空间</th>
                                <td><?php echo htmlentities($serverinfo['disk_free_space']); ?></td></tr>
                            <tr>
                                <th>文档路径</th>
                                <td><?php echo htmlentities($serverinfo['document_root']); ?></td></tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="layui-col-md12">
                <div class="layui-card">
                    <div class="layui-card-header">开发团队</div>
                    <div class="layui-card-body ">
                        <table class="layui-table">
                            <tbody>
                            <tr>
                                <th>版权所有</th>
                                <td><?php echo htmlentities($serverinfo['url']); ?>(水月居)
                                    <a href="http://www.zjttxwgy.com/" target="_blank">访问官网</a></td>
                            </tr>
                            <tr>
                                <th>开发者</th>
                                <td>水月居(87807564@qq.com)</td></tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <style id="welcome_style"></style>
            <div class="layui-col-md12">
                <blockquote class="layui-elem-quote layui-quote-nm">感谢layui,百度Echarts,jquery,本系统由x-admin提供技术支持。</blockquote></div>
        </div>
    </div>
    </div>
    </body>


</html>