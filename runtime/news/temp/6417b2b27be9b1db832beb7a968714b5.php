<?php /*a:2:{s:52:"D:\WWW6\tp6-xadmin\app\news\view\category\index.html";i:1604015142;s:49:"D:\WWW6\tp6-xadmin\app\news\view\layout\base.html";i:1604035553;}*/ ?>
<!doctype html>
<html lang="zh-CN">
<head>
    <meta charset="utf-8">
    <title><?php echo htmlentities($new_category['title']); ?>-<?php echo htmlentities($confs['site']['title_second']); ?></title>
    <meta name="keywords" content="<?php echo htmlentities($confs['site']['keywords']); ?>">
    <meta name="description" content="<?php echo htmlentities($new_category['desc']); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="/static/css/base.css" rel="stylesheet">
    <link href="/static/css/m.css" rel="stylesheet">
    <script src="/static/js/jquery-1.8.3.min.js"></script>
    
    <script src="/static/js/comm.js"></script>
    <!--[if lt IE 9]>
    <script src="/static/js/modernizr.js"></script>
    <![endif]-->
</head>
<body>
<!--top begin-->
<header id="header">
    <div class="navbox">
        <h2 id="mnavh"><span class="navicon"></span></h2>
        <div class="logo"><a href="/" title="<?php echo htmlentities($confs['site']['title']); ?>-<?php echo htmlentities($confs['site']['title_second']); ?>"><img src="<?php echo htmlentities($confs['site']['logo']); ?>"></a></div>
        <nav>
            <ul id="starlist">
                <li><a href="/news/" title="水月居">资讯</a></li>
                <?php if(is_array($navs) || $navs instanceof \think\Collection || $navs instanceof \think\Paginator): $i = 0; $__LIST__ = $navs;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$nav): $mod = ($i % 2 );++$i;?>
                    <li class="<?php echo !empty($nav['children']) ? 'menu' : ''; ?>">
                        <a href="/news/category/<?php echo htmlentities((isset($nav['id']) && ($nav['id'] !== '')?$nav['id']:'javascript:;')); ?>.html"><?php echo htmlentities($nav['title']); ?></a>
                        <?php if($nav['children']): ?>
                            <ul class="sub">
                                <?php if(is_array($nav['children']) || $nav['children'] instanceof \think\Collection || $nav['children'] instanceof \think\Paginator): $i = 0; $__LIST__ = $nav['children'];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$m): $mod = ($i % 2 );++$i;?>
                                    <li><a href="/news/category/<?php echo htmlentities((isset($m['id']) && ($m['id'] !== '')?$m['id']:'javascript:;')); ?>.html"><?php echo htmlentities($m['title']); ?></a></li>
                                <?php endforeach; endif; else: echo "" ;endif; ?>
                            </ul><span></span>
                        <?php endif; ?>
                    </li>
                <?php endforeach; endif; else: echo "" ;endif; ?>
            </ul>
        </nav>
        <div class="searchico"></div>
    </div>
</header>
<div class="searchbox">
    <div class="search">
        <form action="<?php echo url('/news/article.search',['page'=>1]); ?>" method="get" name="searchform" id="searchform">
            <input name="keyboard" id="keyboard" class="input_text" placeholder="请输入关键字词" type="text">
            <input class="input_submit" type="submit">
        </form>
    </div>
    <div class="searchclose"></div>
</div>
<!--top end-->
<article>
    
    <div class="lbox">
        <div class="whitebg lanmu"><img src="/static/picture/lm01.jpg">
            <h1><?php echo htmlentities($new_category['title']); ?></h1>
            <p><?php echo htmlentities($new_category['desc']); ?></p>
        </div>
        <div class="whitebg bloglist">
            <h2 class="htitle"><?php echo htmlentities($new_category['title']); ?></h2>
            <ul>
                <?php if(is_array($news) || $news instanceof \think\Collection || $news instanceof \think\Paginator): $i = 0; $__LIST__ = $news;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?>
                    <li>
                        <h3 class="blogtitle">
                            <!--<b>【顶】</b>-->
                            <a href="<?php echo url('/news/article/index',['id'=>$vo['id']]); ?>" target="_blank"><?php echo htmlentities($vo['title']); ?></a>
                        </h3>
                        <span class="blogpic imgscale">
                            <i><a href="<?php echo url('/news.category/index',['id'=>$vo['id'],'page'=>1]); ?>" target="_blank"><?php echo htmlentities($new_category['title']); ?></a></i>
                            <a href="<?php echo url('/news.article/index',['id'=>$vo['id']]); ?>" title="<?php echo htmlentities($vo['title']); ?>">
                                <img src="<?php echo htmlentities($vo['cover']); ?>" alt="<?php echo htmlentities($vo['title']); ?>">
                            </a>
                        </span>
                        <p class="blogtext">
                            <?php echo htmlentities($vo['abstract']); ?>
                        </p>
                        <p class="bloginfo">
                            <i class="avatar"><img src="/static/picture/avatar.jpg"></i><span></span>
                            <span><?php echo htmlentities($vo['create_time']); ?></span>
                            <span>【<a href="/news/category/<?php echo htmlentities($vo['newsCategory']['id']); ?>.html" target="_blank"><?php echo htmlentities($vo['newsCategory']['title']); ?></a>】</span>
                        </p>
                        <a href="<?php echo url('/news/article/index',['id'=>$vo['id']]); ?>" class="viewmore">阅读更多</a>
                    </li>
                <?php endforeach; endif; else: echo "" ;endif; ?>
            </ul>
            <!--pagelist-->
            <?php echo $news; ?>
            <!--pagelist end-->
        </div>
        <!--bloglist end-->
    </div>

    
    <div class="rbox">
        <?php echo $sidebar->render(); ?>
    </div>

</article>

<footer>
    <div class="box">
        <div class="wxbox">
            <ul>
                <li><img src="<?php echo htmlentities($confs['base']['weixin_image']); ?>"><span>微信公众号</span></li>
                <li><img src="<?php echo htmlentities($confs['base']['weixin_mp']); ?>"><span>我的微信</span></li>
            </ul>
        </div>
        <div class="endnav">
            <p><b>站点声明：</b></p>
            <p>1、本站个人博客模板，个人可以使用，但是未经许可不得用于任何商业目的。</p>
            <p>2、所有文章未经授权禁止转载、摘编、复制或建立镜像，如有违反，追究法律责任。</p>
            <p><?php echo htmlentities($confs['site']['copyright']); ?> 备案号：<?php echo htmlentities($confs['site']['record_no']); ?></p>
        </div>
    </div>
    <a href="#">
        <div class="top"></div>
    </a>
</footer>

<div style="display: none">
    <script type="text/javascript" src=""></script>
</div>
</body>
</html>