<?php /*a:2:{s:53:"D:\WWW6\thinkphp6cms\app\news\view\article\index.html";i:1603125077;s:51:"D:\WWW6\thinkphp6cms\app\news\view\layout\base.html";i:1604035553;}*/ ?>
<!doctype html>
<html lang="zh-CN">
<head>
    <meta charset="utf-8">
    <title><?php echo htmlentities($article['title']); ?>-<?php echo htmlentities($confs['site']['title_second']); ?></title>
    <meta name="keywords" content="<?php echo htmlentities($confs['site']['keywords']); ?>">
    <meta name="description" content="<?php echo htmlentities($article['abstract']); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="/static/css/base.css" rel="stylesheet">
    <link href="/static/css/m.css" rel="stylesheet">
    <script src="/static/js/jquery-1.8.3.min.js"></script>
    
    <script src="/static/js/comm.js"></script>
    <!--[if lt IE 9]>
    <script src="/static/js/modernizr.js"></script>
    <![endif]-->
</head>
<body>
<!--top begin-->
<header id="header">
    <div class="navbox">
        <h2 id="mnavh"><span class="navicon"></span></h2>
        <div class="logo"><a href="/" title="<?php echo htmlentities($confs['site']['title']); ?>-<?php echo htmlentities($confs['site']['title_second']); ?>"><img src="<?php echo htmlentities($confs['site']['logo']); ?>"></a></div>
        <nav>
            <ul id="starlist">
                <li><a href="/news/" title="水月居">资讯</a></li>
                <?php if(is_array($navs) || $navs instanceof \think\Collection || $navs instanceof \think\Paginator): $i = 0; $__LIST__ = $navs;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$nav): $mod = ($i % 2 );++$i;?>
                    <li class="<?php echo !empty($nav['children']) ? 'menu' : ''; ?>">
                        <a href="/news/category/<?php echo htmlentities((isset($nav['id']) && ($nav['id'] !== '')?$nav['id']:'javascript:;')); ?>.html"><?php echo htmlentities($nav['title']); ?></a>
                        <?php if($nav['children']): ?>
                            <ul class="sub">
                                <?php if(is_array($nav['children']) || $nav['children'] instanceof \think\Collection || $nav['children'] instanceof \think\Paginator): $i = 0; $__LIST__ = $nav['children'];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$m): $mod = ($i % 2 );++$i;?>
                                    <li><a href="/news/category/<?php echo htmlentities((isset($m['id']) && ($m['id'] !== '')?$m['id']:'javascript:;')); ?>.html"><?php echo htmlentities($m['title']); ?></a></li>
                                <?php endforeach; endif; else: echo "" ;endif; ?>
                            </ul><span></span>
                        <?php endif; ?>
                    </li>
                <?php endforeach; endif; else: echo "" ;endif; ?>
            </ul>
        </nav>
        <div class="searchico"></div>
    </div>
</header>
<div class="searchbox">
    <div class="search">
        <form action="<?php echo url('/news/article.search',['page'=>1]); ?>" method="get" name="searchform" id="searchform">
            <input name="keyboard" id="keyboard" class="input_text" placeholder="请输入关键字词" type="text">
            <input class="input_submit" type="submit">
        </form>
    </div>
    <div class="searchclose"></div>
</div>
<!--top end-->
<article>
    
    <div class="lbox">
        <div class="content_box whitebg">
            <h2 class="htitle"><span class="con_nav">您现在的位置是：<a href="/">首页</a>&nbsp;>&nbsp;<a
                    href="/news/category/<?php echo htmlentities($article['newsCategory']['id']); ?>/page/1"><?php echo htmlentities($article['newsCategory']['title']); ?></a></span><?php echo htmlentities($article['newsCategory']['title']); ?></h2>
            <h1 class="con_tilte"><?php echo htmlentities($article['title']); ?></h1>
            <p class="bloginfo">
                <i class="avatar"><img src="/static/picture/avatar.jpg"></i>
                <span>上善若水</span><span><?php echo htmlentities($article['create_time']); ?></span><span>【<?php echo htmlentities($article['newsCategory']['title']); ?>】</span>
                <span><?php echo htmlentities($article['browse']); ?>人已围观</span>
            </p>
            <p class="con_info"><b>简介</b><?php echo htmlentities($article['abstract']); ?></p>
            <div class="con_text">
                <p>
                    <?php echo $article['content']; ?>
                </p>
                <?php if(!(empty($article['tags']) || (($article['tags'] instanceof \think\Collection || $article['tags'] instanceof \think\Paginator ) && $article['tags']->isEmpty()))): ?>
                    <p>Tags：
                        <?php if(is_array($article['tags']) || $article['tags'] instanceof \think\Collection || $article['tags'] instanceof \think\Paginator): $i = 0; $__LIST__ = $article['tags'];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?>
                            <a href="<?php echo url('tag.index',['name'=>$vo['name'],'page'=>1]); ?>"><?php echo htmlentities($vo['name']); ?></a>
                        <?php endforeach; endif; else: echo "" ;endif; ?>
                    </p>
                <?php endif; ?>
                <p class="share"><b>转载：</b>感谢您对思议岁月个人博客网站平台的认可，以及对我们原创作品以及文章的青睐，非常欢迎各位朋友分享到个人站长或者朋友圈，但转载请说明文章出处“来源思议岁月个人博客”。</p>
                <p><span class="diggit">
                <a href="javascript:;" data-id="<?php echo htmlentities($article['id']); ?>" id="diggnum"> 很赞哦！ </a>(<b id="diggnum_b"><?php echo htmlentities($article['up']); ?></b>)</span>
                </p>
                <div class="nextinfo">
                    <p>上一篇：<?php if(empty($up_article) || (($up_article instanceof \think\Collection || $up_article instanceof \think\Paginator ) && $up_article->isEmpty())): ?>
                        <span>没有上一篇</span>
                        <?php else: ?>
                        <a href="<?php echo url('article/index',['id'=>$up_article['id']]); ?>"><?php echo htmlentities($up_article['title']); ?></a>
                    <?php endif; ?>
                    </p>
                    <p>下一篇：<?php if(empty($down_article) || (($down_article instanceof \think\Collection || $down_article instanceof \think\Paginator ) && $down_article->isEmpty())): ?>
                        <span>没有下一篇</span>
                        <?php else: ?>
                        <a href="<?php echo url('article/index',['id'=>$down_article['id']]); ?>"><?php echo htmlentities($down_article['title']); ?></a>
                    <?php endif; ?>
                    </p>
                </div>
            </div>
        </div>
        <div class="whitebg">
            <h2 class="htitle">相关文章</h2>
            <ul class="otherlink">
                <?php if(is_array($relevant_news) || $relevant_news instanceof \think\Collection || $relevant_news instanceof \think\Paginator): $i = 0; $__LIST__ = $relevant_news;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?>
                    <li><a href="<?php echo url('article/index',['id'=>$vo['id']]); ?>" title="<?php echo htmlentities($vo['title']); ?>" target="_blank"><?php echo htmlentities($vo['title']); ?></a></li>
                <?php endforeach; endif; else: echo "" ;endif; ?>
            </ul>
        </div>
        <div class="whitebg">
            <h2 class="htitle">随机图文</h2>
            <ul class="xiangsi">
                <?php if(is_array($rand_news) || $rand_news instanceof \think\Collection || $rand_news instanceof \think\Paginator): $i = 0; $__LIST__ = $rand_news;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?>
                    <li><a href="<?php echo url('article/index',['id'=>$vo['id']]); ?>" target="_blank"><i><img
                            src="<?php echo htmlentities($vo['cover']); ?>"></i>
                        <p><?php echo htmlentities($vo['title']); ?></p>
                        <span><?php echo htmlentities($vo['abstract']); ?></span></a>
                    </li>
                <?php endforeach; endif; else: echo "" ;endif; ?>
            </ul>
        </div>

        <div class="whitebg gbook">
            <h2 class="htitle">文章评论</h2>
            <ul>
                <?php if(is_array($article['comments']) || $article['comments'] instanceof \think\Collection || $article['comments'] instanceof \think\Paginator): $i = 0; $__LIST__ = $article['comments'];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?>
                    <div class="fb"><ul><p class="fbtime"><span><?php echo htmlentities($vo['create_time']); ?></span> <?php echo htmlentities($vo['username']); ?></p><p class="fbinfo"><?php echo htmlentities($vo['content']); ?></p></ul></div>
                <?php endforeach; endif; else: echo "" ;endif; ?>
                <form action="/comment/add" method="post" name="saypl" id="saypl">
                    <div id="plpost">
                        <p class="saying"><span><a href="javascript:;">共有<?php echo htmlentities($article['comments_count']); ?>条评论</a></span>来说两句吧...</p>
                        <p class="yname"><span>用户名:</span>
                            <input name="username" type="text" class="inputText" id="username" value="" size="16">
                        </p>
                        <p class="yzm"><span>验证码:</span>
                            <input name="captcha" type="text" class="inputText" size="16" id="captcha">
                            <img src="/article/verify" align="absmiddle" alt="captcha" id="plKeyImg" title="看不清楚,点击刷新">
                        </p>
                        <textarea name="content" rows="6" id="saytext"></textarea>
                        <input name="news_id" type="hidden" value="<?php echo htmlentities($article['id']); ?>" id="news_id">
                        <input type="submit" class="submit_comment" value="提交">
                    </div>
                </form>
            </ul>
        </div>
    </div>

    
    <div class="rbox">
        <?php echo $sidebar->render(); ?>
    </div>

</article>

<footer>
    <div class="box">
        <div class="wxbox">
            <ul>
                <li><img src="<?php echo htmlentities($confs['base']['weixin_image']); ?>"><span>微信公众号</span></li>
                <li><img src="<?php echo htmlentities($confs['base']['weixin_mp']); ?>"><span>我的微信</span></li>
            </ul>
        </div>
        <div class="endnav">
            <p><b>站点声明：</b></p>
            <p>1、本站个人博客模板，个人可以使用，但是未经许可不得用于任何商业目的。</p>
            <p>2、所有文章未经授权禁止转载、摘编、复制或建立镜像，如有违反，追究法律责任。</p>
            <p><?php echo htmlentities($confs['site']['copyright']); ?> 备案号：<?php echo htmlentities($confs['site']['record_no']); ?></p>
        </div>
    </div>
    <a href="#">
        <div class="top"></div>
    </a>
</footer>

    <script>
        $(function () {
            $('#diggnum').click(function () {
                var $_this=$(this);
                var already=$_this.attr('already');
                if(already){
                    alert('你已经赞过该文章了，谢谢支持！');
                }else {
                    var id=$_this.data('id');
                    $.get('/article/zan',{id:id},function () {
                        var $b=$('#diggnum_b');
                        $_this.attr('already','true');
                        $b.text(parseInt($b.text())+1);
                        alert('点赞成功，谢谢支持！');
                    });
                }
            });
            $('#plKeyImg').click(function () {
                $(this).attr('src','/article/verify?rand='+Math.random())
            });
            $('.submit_comment').click(function () {
                var username=$('#username').val();
                var captcha=$('#captcha').val();
                var content=$('#saytext').val();
                var news_id=$('#news_id').val();
                var json={username:username,captcha:captcha,content:content,news_id:news_id};
                $.post('/comment/add',json,function (res) {
                    if(res.code==0){
                        alert('提交成功！');
                        window.location.reload();
                    }else {
                        alert(res.msg);
                        $('#plKeyImg').click();
                    }
                })
                return false;
            });
        })
    </script>

<div style="display: none">
    <script type="text/javascript" src=""></script>
</div>
</body>
</html>