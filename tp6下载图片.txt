thinkPHP自带的图片批量打包扩展ZipArchive
weixin_30252155 2019-05-17 14:45:00 53 收藏
文章标签： php
版权

public function downImg1()
{
    //在此之前你的项目目录中必须新建一个空的zip包
    $file_template='../public/canddata/cand_picture.zip';
    //自定义文件名
    $card="yezi叶";
    //即将打包的zip文件名称
    $downname = $card.'.zip';
    //把你打包后zip所存放的目录
    $file_name = "../public/canddata/".$card."zip";
    //把原来项目目录存在的zip复制一份新的到另外一个目录并重命名（可以在原来的目录）
    $result = copy( $file_template, $file_name );
    $zip=new ZipArchive();

    $imgArr= array(
        array('image_src' => 'D:\wamp64\www\tp5\public\images\3.jpg', 'image_name' => 'pic/图片1.jpg'),
        array('image_src' => 'D:\wamp64\www\tp5\public\images\yahoo.gif', 'image_name' => 'pic/图片2.jpg'),
    );
    dump($zip);
    //打开你复制过后空的zip,包在zip压缩包中建一个空文件夹，成功时返回 TRUE
    if($zip->open($file_name,ZipArchive::CREATE)===TRUE){
        $zip->addEmptyDir($card);
        //根据自己的场景需要去处理业务
        $i=1;
        foreach($imgArr as $key=>$val){
            //得到图片后缀名
            $file_ext=explode('.',$val['image_src']);
            //图片重命名
            $zip->addFromString($card.'/'.$card.'_'.$i.'.'.$file_ext[1],file_get_contents($val['image_src']));
            $i++;
        }
        $zip->close();
        $fp=fopen($file_name,"r");
        //获取文件的字节
        $file_size=filesize($file_name);
        //下载需要的header头
        Header("Content-type: application/octet-stream");
        Header("Accept-Ranges: bytes");
        Header("Accept-Length:".$file_size);
        Header("Content-Disposition: attachment; filename=$downname");
        //设置一次读取的字节数，每读取一次，就输出数据（即返回给浏览器）
        $buffer=1024;
        //读取的总字节数
        $file_count=0;
        //向浏览器返回数据，如果下载完成就停止输出
        while(!feof($fp) && $file_count<$file_size){
            $file_con=fread($fp,$buffer);
            $file_count+=$buffer;
            echo $file_con;
        }
        fclose($fp);
        //下载完成后删除压缩包，临时文件夹
        if($file_count>=$file_size){
            unlink($file_name);
        }
    }

}

