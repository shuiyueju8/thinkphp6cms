<?php
// +----------------------------------------------------------------------
// | 控制台配置
// +----------------------------------------------------------------------
return [
    // 指令定义
    'commands' => [
        "joke_update"=>"app\command\JokeUpdate",
        "collect_task"=>"app\command\CollectTaskCommand",
        "cover_pre_update"=>"app\command\CoverPreUpdate",
        "duanzi_update"=>"app\command\DuanziUpdate",
        "day_run"=>"app\command\DayRun",
    ],
];
