<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------
use think\facade\Route;

//个人博客日记
Route::get('blog/:page', 'Category/index')->append(['id' => 1])->name('category.blog')->ext('html');
//技术交流
Route::get('technology/:page', 'Category/index')->append(['id' => 2])->name('category.technology')->ext('html');
//PHP框架
Route::get('framework/:page', 'Category/index')->append(['id' => 3])->name('category.framework')->ext('html');
//网站公告
Route::get('notice/:page', 'Category/index')->append(['id' => 4])->name('category.notice')->ext('html');
//thinkphp6教程
Route::get('thinkphp6/:page', 'Category/index')->append(['id' => 5])->name('category.thinkphp6')->ext('html');
//幸福生活
Route::get('happy_life/:page', 'Category/index')->append(['id' => 6])->name('category.happy_life')->ext('html');
//幽默故事
Route::get('humorous_story/:page', 'Category/index')->append(['id' => 7])->name('category.humorous_story')->ext('html');
//快乐段子
Route::get('happy_quotes/:page', 'Category/index')->append(['id' => 8])->name('category.happy_quotes')->ext('html');
//涨姿势
Route::get('rising_knowledge/:page', 'Category/index')->append(['id' => 9])->name('category.rising_knowledge')->ext('html');
//福利分享
Route::get('welfare_share/:page', 'Category/index')->append(['id' => 10])->name('category.welfare_share')->ext('html');
//暖心鸡汤
Route::get('soul_soother/:page', 'Category/index')->append(['id' => 11])->name('category.soul_soother')->ext('html');
//心情随笔
Route::get('essay/:page', 'Category/index')->append(['id' => 12])->name('category.essay')->ext('html');
//经验之谈
Route::get('experience/:page', 'Category/index')->append(['id' => 13])->name('category.experience')->ext('html');
//经验之谈
Route::get('java/:page', 'Category/index')->append(['id' => 14])->name('category.java')->ext('html');
//article
Route::get('article/:id', 'Article/index')->name('article.index')->ext('html');
//文章标签路由
Route::get('tag/:name/:page', 'Tag/index')->name('tag.index')->ext('html');
//文章标签路由
Route::get('search/:page', 'Article/search')->name('article.search')->ext('html');