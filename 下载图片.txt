
thinkphp 批量打包下载图片到本地电脑
O-Striue-O 2018-08-10 16:13:33 1365 收藏
文章标签： php readfile 批量下载
版权

    //批量下载图片
        public function picDownload(){
            if(IS_GET) {
    //            phpinfo();
    //            exit;
                $type = I('get.type', 0, 'intval');
                $bid = I('get.bid', 0, 'intval');
                $images = M('images')->where(array('bid'=>$bid,'type'=>$type,'del'=>0))->select();
     
                $filename = "./Uploads/" . date ( 'YmdH' ) . ".zip";
                // 生成文件
                $zip = new \ZipArchive ();
                // 使用本类，linux需开启zlib，windows需取消php_zip.dll前的注释
                if ($zip->open ($filename ,\ZipArchive::OVERWRITE) !== true) {
                    //OVERWRITE 参数会覆写压缩包的文件 文件必须已经存在
                    if($zip->open ($filename ,\ZipArchive::CREATE) !== true){
                        // 文件不存在则生成一个新的文件 用CREATE打开文件会追加内容至zip
                        exit ( '无法打开文件，或者文件创建失败' );
                    }
                }
     
                foreach($images as $key => $v){
                    $v['swfimglist'] =  substr($v['swfimglist'],1);
                    $zip->addEmptyDir("attach");
                    if(file_exists($v['swfimglist'])){
                        $zip->addFile($v['swfimglist'], basename($v['swfimglist']));
                    } else {
    //                    die('图片地址不对哦');
                        echo 'error';
                        exit;
                    }
                }
            }
            // 关闭
            $zip->close ();
            //下面是输出下载;
            header ( "Cache-Control: max-age=0" );
            header ( "Content-Description: File Transfer" );
            header ( 'Content-disposition: attachment; filename=' . basename ( $filename ) ); // 文件名
            header ( "Content-Type: application/zip" ); // zip格式的
            header ( "Content-Transfer-Encoding: binary" ); // 告诉浏览器，这是二进制文件
            header ( 'Content-Length: ' . filesize ( $filename ) ); // 告诉浏览器，文件大小
            @readfile ( $filename );//输出文件;
                exit;
            }

本地环境要开启ZIP；linux需开启zlib，并且创建文件的目录权限要有写入权限
